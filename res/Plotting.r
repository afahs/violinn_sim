
library(dplyr)
library(ggplot2)
library(tidyr)

#setwd("") # use this line to set your home directory as FOLDER_LOCATION/voila-orch_sim/res
DF=read.csv("./res_n_100_L0_20_C0__Bias_50_PS_8.0.csv", header = TRUE, stringsAsFactors = FALSE)


SDF = DF %>% select(Cycle,Size,Algo,NS,Cost,AEp,TotalReq) # eliminate some variables
SDF <- SDF %>% gather(Key, Value,Size,NS,Cost,AEp,TotalReq) # organize the data as keys and values. 


#SDF<- SDF[which(SDF$Algo!="IgnoreS"),] # run this line to compare the algoreithms. 
SDF<- SDF[which(SDF$Algo=="IgnoreS" | SDF$Algo=="QoSOnly"),] # run this line to compare the With S and Ignore S. 


#facet_grid(All ~ ., scales = "free_y",labeller=as_labeller(n_names))+


plot<-ggplot(transform(SDF, Key=factor(Key,levels=c("TotalReq","NS","Size","Cost","AEp"))), aes(x=Cycle))+
  geom_point(size=3, aes(y=Value, colour = Algo,shape=Algo))+
  facet_grid(Key ~ ., scales = "free_y")+
  geom_line(aes(y=Value, colour = Algo))+
  theme_bw()

plot

# Use the following line to save the plot
#s=as.integer(Sys.time())
#s=paste0("plot_",s,".pdf")
#print(paste0("Saving the current plot as ",s))
#ggsave(path=getwd(),filename = s,plot,width = 10,height = 8,units = "in") # change the filename if you do not mind replacing the file at each execution

