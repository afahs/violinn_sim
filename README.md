This is the implementation code used in the simulator evaluation of the following work:

VioLinn: Proximity-aware Edge Placement with Dynamic and Elastic Resource Provisioning
Toczé K, Fahs A J, Pierre G, Nadjm-Tehrani S.
Accepted in ACM Transactions on Internet of Things

Please cite the above article if reusing/building upon our work. 
