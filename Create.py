#!/usr/bin/python3

####STD LIBS#######
import random
import numpy as np
import pickle as pk
from statistics import stdev
####### LIBS#######
import Coor as co
###################

################################ Virtual Nodes #########################################
class node:
	"""
	Nodes class that contain the node variables: 
		name 	=> Name of the node STRING
		req 	=> Number of the requests received from end users INT
		master 	=> Describe if the node is a master of not BOOL
		vec		=> Vivaldi coordinates ARRAY OF FLOAT
		adj 	=> Adjustment variable described by the Vivaldi FLOAT
		h 		=> Height from the space FLOAT
		type	=> Node type: dedicated (D) or spare (S)
	"""
	def __init__(self,name,Req,Idle,vec,adj,h):
		self.name     =  name     				
		self.req      =  Req
		self.master   = False
		self.Idle 	  = Idle 
		self.vec=vec
		self.adj=adj
		self.h=h
		self.type="D"


def CreateNodes(Number,Dynamic,Dist,Load,Dim,Dispersion,Bias,mean,Variance,numberSpareNodes):
	"""
	A function that creates the Artificial nodes
		Number 		=> Number of nodes
		Dynamic		=> Dynamic load created by the LoadData or static load
		Dist		=> The type of probabilistic distribution for the latency
		Load		=> Parameters of the load per node
		Dim 		=> Dimension of the Vivaldi Euclidean Space
		Dispersion	=> Describes the spread of the node in the space
	"""
	Nodes=[]

	if Dynamic: 
		for i in range(Number): 
			name="node"+str(i+1)
			if random.randint(1,101)>Bias:
				Req=1
				Idle=False
			else: 
				Req=0
				Idle=True
			vec=[]
			adj=0
			h=0
			Nodes.append(node(name,Req,Idle,vec,adj,h))

	else: 
		assert Dist=="uniform" or Dist=="normal", "the distribution function is unknown \"%s\"" % Dist

		if Dist=="uniform": 
			func=np.random.uniform
			attr=(-Dispersion,Dispersion)
			attradj	=(-0.0001,0.0001) # for further investigation
			attrh	=(0,0.001)  # for further investigation

		else: 
			func 	=np.random.normal
			attr 	=(0,Dispersion)
			attradj	=(0,0.0001)  # for further investigation
			attrh	=(0,0.002)  # for further investigation

		for i in range(Number):
			name="node"+str(i+1) 
			Req=random.randint(Load[0],Load[1]) #Random number of requests (TO BE Changed)
			vec=[] #creating random Vivaldi Coordinates
			
			for i in range(Dim): # Create random location for the node
				vec.append(func(attr[0],attr[1]))
				adj=func(attradj[0],attradj[1])
				h=abs(func(attrh[0],attrh[1]))

			Nodes.append(node(name,Req,vec,adj,h))
		CreateDistributedLoad(Nodes,Bias,mean,Variance)

		Nodes[0].master=True # make the first node a master node
	
	## Transform part of the nodes into spare nodes (the numberSpareNodes last ones in the node list are considered spare)
	spare=range(Number-numberSpareNodes,Number)
	for node_s in spare:
		Nodes[node_s].type="S"
		#Spare nodes do not generate load so they are set as Idle
		Nodes[node_s].Idle=True
	return Nodes

################################ Virtual Latencies #########################################
def CalculateLatencies(Nodes):
	"""
	Calculates the Latency between the nodes using Vivaldi
	The returned value in ms 
	"""
	latencies={}
	for node in Nodes:
		latencies[node.name]=[]
		for n in Nodes: 
			if n != node: 
				latencies[node.name].append((n.name,float(format(co.CalculateADistance(node,n),'.2f'))))
	for node in Nodes: 
		latencies[node.name].append((node.name,0.2)) # the local hos latency should be 0.2 instead of 0 
	return latencies


def CreateDistributedLoad(Nodes,Bias,mean,Variance):
	"""
	"""
	for node in Nodes: 
		if random.randint(1,101)>Bias: 
			node.req=abs(int(random.normalvariate(mean, Variance)))
		else: 
			node.req=0

def CalculateAverageInterNodeLatencies(latencies):
	values={}
	All=[]
	for (k,v) in latencies.items():
		for (k2,v2) in v:
			All.append(v2)
	All.sort()
	values["Mean"]   =  sum(All)/len(All)
	values["Median"]    =  All[int(len(All)/2)]
	values["Variance"]  = stdev(All)
	return values

######################################## MAIN ##############################################
def Create(numbernodes,numberSpareNodes,Dynamic=True,Dist="uniform",Load=(1000,5000),Dim=3,Dispersion=0.008,Bias=80,mean=300,Variance=300):
	"""
	A function that creates the Artificial nodes and latencies
	then save them in pickle files.
	"""
	assert numbernodes>numberSpareNodes, "Number of Spare Nodes exceeds the number of Nodes"
	Nodes=CreateNodes(numbernodes,Dynamic,Dist,Load,Dim,Dispersion,Bias,mean,Variance,numberSpareNodes)

	with open('./temp/node.pk', 'wb') as f:
		pk.dump(Nodes,f, pk.HIGHEST_PROTOCOL)
	if not Dynamic: 
		latencies=CalculateLatencies(Nodes)
		with open('./temp/lat.pk', 'wb') as f:
			pk.dump(latencies, f, pk.HIGHEST_PROTOCOL)
