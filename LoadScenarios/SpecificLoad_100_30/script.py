#!/usr/bin/python3

####STD LIBS#######
import pandas as pd
###################

## Transform the modified load_df csv file into a pk file

csvData=pd.read_csv("load_df.csv")
csvData.to_pickle("dfhour.pk")
