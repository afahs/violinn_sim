#!/usr/bin/python3

####STD LIBS#######
import random
import numpy as np
import pickle as pk
import time
import math
from statistics import stdev
####### LIBS#######
import hona
import Coor as co
###################


############################# Pickle Load #############################################

def PickleLoad(filename):
	pickledlist=[]
	with open(filename, 'rb') as f:
		pickledlist = pk.load(f)
	return pickledlist

def GetNodes():
	Nodes=PickleLoad("./temp/node.pk")
	return Nodes

def GetLatencies():
	latencies=PickleLoad("./temp/lat.pk")
	return latencies



def CalculateAverageInterNodeLatencies(latencies):
	values={}
	All=[]
	for (k,v) in latencies.items():
		for (k2,v2) in v:
			All.append(v2)
	All.sort()
	values["Mean"]   =  sum(All)/len(All)
	values["Median"]    =  All[int(len(All)/2)]
	return values


def GetBest(Replicas,tech,L,vres=99,vfair=5,vtime=5,vcount=20,method="Mean",algo="Vivaldi",Change=1):
	a=0.97
	Nodes=GetNodes()
	latencies=GetLatencies()
	if algo=="Vivaldi":
		
		Cases,timeout=CreateVivaldiCases(Nodes,latencies,Replicas,tech,L,vres=vres,vfair=vfair,vtime=vtime,vcount=vcount,method=method,Change=Change)
		Min,Max=GetMinMax(Cases)
		for c in Cases: 
			c.d=a*(c.res/Max)+(1-a)*(Min/c.fair)
			# if c.fair<vfair: 
			# 	c.d+=0.5
			# if c.res>vres: 
			# 	c.d+=0.5
		best=GetBestD(Cases)
		return best,Cases,timeout
	if algo=="Random":
		Cases,timeout=CreateRandomCases(Nodes,latencies,Replicas,vres=vres,vfair=vfair,vtime=vtime,vcount=vcount,method=method)
		Min,Max=GetMinMax(Cases)
		for c in Cases: 
			c.d=a*(c.res/Max)+(1-a)*(Min/c.fair)
			if c.fair<vfair: 
				c.d+=0.5
			if c.res>vres: 
				c.d+=0.5
		best=GetBestD(Cases)
		return best,Cases,timeout
	else:
		Cases,timeout=CreateRandomCases(Nodes,latencies,Replicas,vres=vres,vfair=vfair,vtime=vtime,vcount=vcount,method=method)
		return Cases[0],Cases,False


def CreateVivaldiCases(Nodes,latencies,n,tech,L,vres=99,vfair=5,vtime=5,vcount=20,method="Mean",Change=1):
	start = time.time()
	values=CalculateAverageInterNodeLatencies(latencies)
	Groups=CreateVivaldiGroups(Nodes,n,latencies,values[method],tech,p1=3,p2=2)
	leaders=GetLeaders(Groups)
	TotalRequests=CalculateTotalRequests(Nodes)
	Percentage=CalculatePercentage(Nodes,TotalRequests)
	i=0
	Cases=[]
	SelectedNodes=GetRandomSet(leaders,n)
	tempcase=case(Nodes,SelectedNodes,i,latencies,Percentage,values,method,L)
	#print(tempcase.res,tempcase.fair,i)
	Cases.append(tempcase)
	i=1
	found,timeout =TestCondition(vres,vfair,vtime,start,tempcase.res,tempcase.fair,i)
	found= not found
	while True:
		i+=1
		SelectedNodes=GetRandomSet(leaders,n) #GetRandomCase(leaders,n)
		Number=i
		tempcase=case(Nodes,SelectedNodes,Number,latencies,Percentage,values,method,L)
		#print(tempcase.res,tempcase.fair,i)
		Cases.append(tempcase)
		if i%60==0: 
			if Change==1:
				#print("change Group")
				Groups=CreateVivaldiGroups(Nodes,n,latencies,values[method],tech,p1=3,p2=2)
				leaders=GetLeaders(Groups)
		if found==False: 
			found,timeout =TestCondition(vres,vfair,vtime,start,tempcase.res,tempcase.fair,i)
			found= not found
		if found==True and i>vcount: 
			break
	return Cases,timeout

def CreateVivaldiGroups(Nodes,n,latencies,value,tech,p1=2,p2=2):
	LatencyValues  = CalculateAverageInterNodeLatencies(latencies)
	TotalRequests  = CalculateTotalRequests(Nodes)
	AverageLoad    = TotalRequests/n
	NodesDensity   = int(len(Nodes)/(n*p1))
	if NodesDensity<2:
		if n*2<len(Nodes):
			NodesDensity=2
	TempNodes=PickleLoad("node.pk")
	Groups         = []
	while len(TempNodes) > 0:
		if len(TempNodes)<NodesDensity:
			Groups.append(group(TempNodes,value,Nodes,tech))
			break
		MainNode=random.choice(TempNodes)
		GroupNodes=[]
		LatenciesFromMain=[]
		for node in TempNodes:
			if node!=MainNode:
				LatenciesFromMain.append((co.CalculateADistance(MainNode,node),node))
		LatenciesFromMain.sort()
		GroupNodes.append(MainNode)
		TempNodes.remove(MainNode)
		for i in range(NodesDensity-1):
			node=LatenciesFromMain[i][1]
			GroupNodes.append(LatenciesFromMain[i][1])
			TempNodes.remove(node)
		Groups.append(group(GroupNodes,value,Nodes,tech))
	return Groups

def GetLeaders(Groups):
	leaders=[]
	for g in Groups:
		if g.leader!=None:
			leaders.append(g.leader)
	return leaders

class group:
	def __init__(self,nodes,value,Nodes,tech):
		self.nodes=nodes
		if tech==0:
			self.getleaderrequests(nodes)
		if tech==1:
			self.getleaderneighbors(nodes,Nodes,value)
	def print(self):
		print(self.leader.name, self.TotalRequests)
		for node in self.nodes: 
			print(node.name,end=",")
		print("")


	def getleaderrequests(self,nodes):
		if not nodes[0].master:
			self.leader=nodes[0]
		else: 
			if len(nodes)>1:
					self.leader=nodes[1]
			else: 
				self.leader=None
		self.TotalRequests=0
		if self.leader!=None:
			for node in nodes:
				self.TotalRequests+=node.req
				if node.req>self.leader.req:
					if not node.master:
						self.leader=node

	def getleaderneighbors(self,nodes,Nodes,value):
		Numberofneighbors={}
		self.TotalRequests=0
		for node in nodes:
			self.TotalRequests+=node.req
			Numberofneighbors[node]=0
		for node in nodes:
			if node.master==True: 
				continue
			for n in Nodes:
				if co.CalculateADistance(n,node) <value: 
					Numberofneighbors[node]+=1
		Numberofneighbors=sorted(Numberofneighbors.items(), key=lambda kv: kv[1], reverse=True)
		topnodes=[]
		first=0
		for node,neighbor in Numberofneighbors:
			if first==0: 
				topnodes.append((node,neighbor))
				first=1
			else: 
				if neighbor==topnodes[0][1]: 
					topnodes.append((node,neighbor))
				else:
					break
		if len(topnodes)==0: 
			self.leader=None
		else:
			if len(topnodes)==1:
				self.leader=topnodes[0][0]
			else:
				maxnode=topnodes[0][0]
				for t in topnodes:
					if maxnode.req<t[0].req: 
						maxnode=t[0]
				self.leader=maxnode  

def getmaxneighbors(Numberofneighbors):
 	if Numberofneighbors[0][0].master==False:
 		best=Numberofneighbors[0]
 	else:
 		if len(Numberofneighbors)>1:
 			best=Numberofneighbors[1]
 		else:
 			return None
 	for l in Numberofneighbors: 
 		if l[1]> best[1] and l[0].master==False:
 			best=l
 	return best

def CalculateTotalRequests(Nodes):
	TotalRequests=0
	for node in Nodes: 
		TotalRequests+=node.req
	return TotalRequests

def CalculatePercentage(Nodes,TotalRequests):
	Percentage={}
	for node in Nodes: 
		Percentage[node.name]=node.req/TotalRequests
	return Percentage

def GetRandomSet(Nodes,n):
	names=[]
	for node in Nodes: 
		names.append(node.name)
	random.shuffle(names)
	return random.sample(names,n)

class case: 
	def __init__(self,Nodes,SelectedNodes,Number,latencies,Percentage,values,method,L):

		self.id    =   Number
		self.nodes =   SelectedNodes
		self.d     =   0
		prob  =   {}
		dist  =   {}
		Result=0
		for node in Nodes:
			prob[node.name]=CalcProb(node.name,SelectedNodes,latencies)

		for node in Nodes: 
			for n in SelectedNodes:
				temp=[]
				temp=list(prob[node.name][n])
				temp[0]=temp[0]*Percentage[node.name]
				prob[node.name][n]=tuple(temp)
				if prob[node.name][n][1]< L:
					Result+=prob[node.name][n][0]
		self.res=Result
		first=0
		for k,v in prob.items():
			if first==0: 
				for k1,v1 in v.items(): 
					dist[k1]=0
					first=1
			for k1,v1 in v.items(): 
				dist[k1]+=v1[0]
		dist1=[]
		for k,v in dist.items():
			dist1.append(v)
		self.fair = stdev(dist1)

def CalcProb(Node,SelectedNodes,latencies):
	lat=[]
	sumall  = 0
	podprob = []
	prob={}
	a=1
	b=1
	for node in SelectedNodes:
			lat.append(GetLat(node,Node,latencies))
	#print(lat)
	for l in lat:
		sumall+= math.exp(-b*l)
	for l in lat: 
		podprob.append(math.exp(-b*l)/sumall)
	for p in podprob:
		podprob[podprob.index(p)]=(a*p+(1-a)*(1/len(SelectedNodes)))*100
	#print("pod probability: "+ str(podprob))
	for i in range(len(SelectedNodes)):
		prob[SelectedNodes[i]]=(podprob[i],lat[i])
	return prob

def GetLat(Node1,Node2,latencies):
	for l in latencies[Node1]:
		if l[0]==Node2:
			return l[1]

def TestCondition(vres,vfair,vtime,start,res,fair,number):
	if number > 1000:
		print("time ended")
		return False,True	
	# if time.time()-start > vtime:
	# 	print("time ended")
	# 	return False,True
	if res>vres:
		if fair<vfair:
			return False,False

	return True,False

def GetMinMax(Cases):
	Min=Cases[0].fair
	Max=Cases[0].res 
	for c in Cases: 
		if c.fair<Min:
			Min=c.fair 
		if c.res>Max:
			Max=c.res
	return Min,Max

def GetBestD(Cases):
	best=Cases[0] 
	for c in Cases: 
		if c.d>best.d: 
			best=c
	return best


def CreateRandomCases(Nodes,latencies,n,vres=99,vfair=5,vtime=5,vcount=100,method="Mean"):
	start = time.time()
	i=2
	Cases=[]
	values=CalculateAverageInterNodeLatencies(latencies)
	TotalRequests=CalculateTotalRequests(Nodes)
	Percentage=CalculatePercentage(Nodes,TotalRequests)
	NewNodes=RemoveMaster(Nodes)
	SelectedNodes=GetRandomSet(NewNodes,n)
	tempcase=case(Nodes,SelectedNodes,0,latencies,Percentage,values,method)
	Cases.append(tempcase)
	if vtime==0:
		i=1001
	#print(tempcase.res,tempcase.fair,i)
	found,timeout=TestCondition(vres,vfair,vtime,start,tempcase.res,tempcase.fair,i)
	while found:
		i+=1
		SelectedNodes=GetRandomSet(NewNodes,n)
		#print(SelectedNodes)
		Number=i
		tempcase=case(Nodes,SelectedNodes,Number,latencies,Percentage,values,method)
		Cases.append(tempcase)
		found,timeout=TestCondition(vres,vfair,vtime,start,tempcase.res,tempcase.fair,i)
		#print(tempcase.res,tempcase.fair,i)
	return Cases,timeout	

def RemoveMaster(Nodes):
	NewNodes=[]
	for node in Nodes:
		if not node.master: 
			NewNodes.append(node)
	return NewNodes