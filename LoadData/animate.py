#!/usr/bin/python3
import sys
sys.path.append('./LoadData/geojson')

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pandas as pd
import time
import organize
import numpy as np
import pickle
import json as js
import collections
import libraries as li

def animate(i):
	df=pd.read_pickle("temp.pk")

	pickle_in = open("mapNode.pk","rb")
	mapNode = pickle.load(pickle_in)
	Nodes=list(df.columns)
	for i in range(len(Nodes)):
		#print(Nodes[i])
		ax1[i].clear()
		ax1[i].set_xlim([0, 24])
		ax1[i].set_ylim([0, 30])
		ax1[i].set_title(Nodes[i]+":"+mapNode[Nodes[i]])
		ax1[i].set_ylabel('# Requests')
		if i == len(Nodes)-1:
			ax1[i].set_xlabel('Time (Hour)')
		ax1[i].plot(df.index,df[Nodes[i]],marker='o')

		if i == len(Nodes)-1:
			ax1[i].set_xlabel('Time (Sec)')
	#ax1[0].clear()
	# ax1[1].clear()
	# ax1[0].set_xlim([0, 24])
	# ax1[0].set_ylim([0, 30])
	# ax1[1].set_xlim([0, 24])
	# ax1[1].set_ylim([0, 30])
	# #ax1.plot(xnew,power_smooth)
	# ax1[0].plot(df.index,df["Frigo1"],marker='o',)
	# ax1[1].plot(df.index,df["Frigo2"],marker='o',)



def animate_status(i):
	df=pd.read_pickle("temp.pk")
	Changes=pd.read_pickle("Changes.pk")
	dfhour=pd.read_pickle("./temp/dfhour.pk")
	#length=dfhour.shape[0]+1
	length=30
	#pickle_in = open("mapNode.pk","rb")
	#mapNode = pickle.load(pickle_in)
	col=["Replica_size","Proximity", "Imbalance", "Total_Load", "Max_Load_Per_Pod","Usage","Cost","NumberS"]
	for i in range(len(col)):
		ax1[i].set_xlim([0,length])

		#print(Nodes[i])
		if i==0: #Replica_size
			ax1[i].clear()
			ax1[i].set_ylim([2, 50])
			#ax1[i].grid()
			#ax1[i].set_title("Proximity")
			ax1[i].set_ylabel("Number_Nodes #")
		if i==1: #Proximity
			ax1[i].clear()
			ax1[i].set_ylim([90, 101])
			#ax1[i].grid()
			#ax1[i].set_title("Proximity")
			ax1[i].set_ylabel("Proximity %")

		if i==2: #Imbalance
			ax1[i].clear()
			ax1[i].set_ylim([0, 30])
			#ax1[i].grid()
			#ax1[i].set_title("Proximity")
			ax1[i].set_ylabel("Imbalance %")

		if i==3: #Total_Load
			ax1[i].clear()
			ax1[i].set_ylim([0, 2000])
			#ax1[i].grid()
			#ax1[i].set_title("Proximity")
			ax1[i].set_ylabel("Total_Load #")

		if i==4: #Max_Load_Per_Pod
			ax1[i].clear()
			ax1[i].set_ylim([0, 300])
			#ax1[i].grid()
			#ax1[i].set_title("Proximity")
			ax1[i].set_ylabel("Max_Load_Per_Pod #")

		if i==5: #Max_Load_Per_Pod
			ax1[i].clear()
			ax1[i].set_ylim([0, 150])
			#ax1[i].grid()
			#ax1[i].set_title("Proximity")
			ax1[i].set_ylabel("Utilization %")

		if i==6: #Max_Load_Per_Pod
			ax1[i].clear()
			ax1[i].set_ylim([0, 25])
			#ax1[i].grid()
			#ax1[i].set_title("Proximity")
			ax1[i].set_ylabel("Cost")


		if i==7: #Max_Load_Per_Pod
			ax1[i].clear()
			ax1[i].set_ylim([0, 10])
			#ax1[i].grid()
			#ax1[i].set_title("Proximity")
			ax1[i].set_ylabel("NumberS")
		
		if i == len(col)-1:
			ax1[i].set_xlabel('Time (Hour)')
		ax1[i].plot(df.index,df[col[i]],marker='o')

		major_ticks = np.arange(0, length, 4)
		minor_ticks = np.arange(0, length, 1)

		ax1[i].set_xticks(major_ticks)
		ax1[i].set_xticks(minor_ticks,minor=True)
		ax1[i].grid(which='both')
		ax1[i].grid(which='minor', alpha=0.2)
		ax1[i].grid(which='major', alpha=0.5)

	for row in Changes.itertuples(): 
		if row[1]=="Proximity": 
			ax1[1].axvline(x=row[0],c="red")
		if row[1]=="Saturation": 
			ax1[4].axvline(x=row[0],c="red")
		if row[1]=="Over": 
			ax1[4].axvline(x=row[0],c="Green")
		if row[2]=="Scale Up": 
			ax1[0].axvline(x=row[0],c="red")
			ax1[0].text(row[0],6,"Up",rotation=-90,c="red")
		if row[2]=="Scale Down": 
			ax1[0].axvline(x=row[0],c="Green")
			ax1[0].text(row[0],6,"Down",rotation=-90,c="Green")
		if row[2]=="Replacement": 
			ax1[0].axvline(x=row[0],c="blue")
			ax1[0].text(row[0],6,"Replace",rotation=-90,c="blue")		





def MapNodes(mapNode,NodeObjects):
	IDS=[v for k,v in mapNode.items()] 
	Co=GetColocatedNodes(mapNode,NodeObjects)
	data=li.LoadGeoJson("./LoadData/trentino-grid.geojson")
	Cells=data['features']
	newcells=[]
	opacity=0.8
	opacity2=1
	for cell in Cells:
		ID=cell["properties"]["cellId"]
		if str(ID) in IDS:
			if str(ID) in Co: #colocated Node
				cell["properties"]["fill"]="#ff7f00"
				cell["properties"]['opacity']= opacity
				cell["properties"]['fillOpacity']= opacity
				newcells.append(cell)
			else: #single Nodes
				cell["properties"]["fill"]="#800026"
				cell["properties"]['opacity']= opacity
				cell["properties"]['fillOpacity']= opacity
				newcells.append(cell)					
		else: 
			cell["properties"]["fill"]="#ffffcc"
			cell["properties"]['opacity']= opacity2
			cell["properties"]['fillOpacity']= opacity2
			newcells.append(cell)
	data['features']=newcells
	li.CreateHtmlSelectedNodes(data,opacity=0.2)
	li.DumpGeoJson("SelectedNodes.geojson",data)

if __name__ == "__main__":
	df=pd.read_pickle("temp.pk")
	fig, ax1 = plt.subplots(len(df.columns))

	ax1[3] = plt.subplot2grid((6, 2), (0, 0), colspan=2)
	ax1[0] = plt.subplot2grid((6, 2), (1, 0), rowspan=2, colspan=2)
	ax1[1] = plt.subplot2grid((6, 2), (3, 0))
	ax1[4] = plt.subplot2grid((6, 2), (3, 1))
	ax1[2] = plt.subplot2grid((6, 2), (4, 0))
	ax1[5] = plt.subplot2grid((6, 2), (4, 1))
	ax1[6] = plt.subplot2grid((6, 2), (5, 0))
	ax1[7] = plt.subplot2grid((6, 2), (5, 1))

	ani = animation.FuncAnimation(fig, animate_status, interval=1000)
	plt.tight_layout()
	plt.show()
	fig.set_size_inches(16,12, forward=True)
	fig.savefig('./result/temp.pdf', dpi=fig.dpi)

def GetColocatedNodes(mapNode,NodeObjects): 
	IDS=[v for k,v in mapNode.items()]
	co=[]
	counter=collections.Counter(IDS)
	for k,v in counter.items(): 
		assert v<3 and v>0, "colocated nodes error, the number of colocated nodes exceeds 2"
		if v==2: 
			co.append(k)
	print("Number of colocated Nodes",len(co)) 
	return co