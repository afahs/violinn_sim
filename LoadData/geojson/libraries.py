#!/usr/bin/python3 

import json as js
import pprint
import sort
import pandas as pd
from math import floor
import folium
import os
import glob
from selenium import webdriver 
import imageio
from PIL import Image, ImageDraw, ImageFont
import calendar
import datetime
###################### DATA #####################
def LoadGeoJson(path): 
	with open(path) as f:
		data = js.load(f)
	return data

def DumpGeoJson(path,geodict):
	with open(path, 'w') as json_file:
		js.dump(geodict, json_file)

def PrepareDf(date,load): 
	# Calculate the requests per each square for
	# one Type of load and one day
	df=sort.LoadPickle("../Pk/"+date+".pk")
	newdf=sort.Requests(df,load)
	newdf.set_index('BS', inplace=True)
	newdf.to_pickle("../RBSSms/"+date+"_RBS2.pk")

################### GEOJSON ####################

def GetColor(Req,Max,Min):
	# Give a colour value in HEX according to 
	# the number of request received by the square
	map={1:"#ffffcc",
	2:"#ffeda0",
	3:"#fed976",
	4:"#feb24c",
	5:"#fd8d3c",
	6:"#fc4e2a",
	7:"#e31a1c",
	8:"#bd0026",
	9:"#800026"}
	print(Min,Max,Req)
	return map[floor(9*(Req-Min+1)/(Max-Min+2))+1]


def HighlightTrento(x=51,y=44):
	# Given a centre coordinates, this function 
	# computes and return the IDList of the squares
	# that are centred by the centre point passed
	IDList=[]
	StartX=x-9
	EndX=x+10
	StartY=y-9
	EndY=y+10
	for i in range(StartX,EndX+1): 
		for j in range(StartY,EndY+1):
			IDList.append(117*j+i+1)
	return(IDList)

def AssignColors(data,df): 
	# Adding the new colours to the GeoJson Code
	Cells=data['features']
	# newcells=[]
	Max=df["#Req"].max()
	Min=df["#Req"].min()
	for cell in Cells:
		ID=cell["properties"]["cellId"]
		if ID in df.index: 
			Req=df.at[ID,"#Req"]
		else: 
			Req=Min
		cell["properties"]["#Req"]=str(Req)
		cell["properties"]["fill"]=GetColor(Req,Max,Min)
	data['features']=Cells
	return data 

def CreateHeatMapTrento(date,x=51,y=44):
	#Creating the Heat map for the city of Trento
	# 20 Km, x,y represent the coordinates of the 
	# centre of the 20 km square (400 Km2 squares)
	# The heat map represents the load for one day  
	df=sort.LoadPickle("../RBS/"+date+"_RBS.pk")
	data=LoadGeoJson("./trentino-grid.geojson")
	IDList=HighlightTrento(x=51,y=44)
	newcells=[]
	Cells=data['features']
	for cell in Cells: 
		if cell["properties"]["cellId"] in IDList: 
			newcells.append(cell)
	data['features']=newcells
	data=AssignColors(data,df)
	DumpGeoJson("./HeatMap_Trento/Trento_"+date+".geojson",data)

def CreateHeatMap(date): 
	# Creating the heat map for the whole Area
	# The heat map represents the load for one day  
	df=sort.LoadPickle("../RBS/"+date+"_RBS.pk")
	data=LoadGeoJson("./trentino-grid.geojson")
	data=AssignColors(data,df)
	DumpGeoJson("./HeatMap/"+date+".geojson",data)

def ListAllDates(): 
	# Return a list of all the available dates
	AllDates=[]
	for i in [1,2]:
		m="1"+str(i)+"-" 
		for j in range(1,32): 
			if j==31 and i==1: 
				continue
			if j<10: 
				d="0"+str(j)
			else: 
				d=str(j)
			date="2013-"+m+d
			AllDates.append(date)
	AllDates.append("2014-01-01")
	return AllDates

###################### HTML ######################

def CreateHtml(date,opacity=0.4):
	data=LoadGeoJson("./HeatMap_Trento/Trento_"+date+".geojson")
	Hmap = folium.Map(location=[46.066666, 11.116667], zoom_start=12)
	folium.GeoJson(data, name="Trento").add_to(Hmap)
	folium.GeoJson(data, name="Trento",   style_function=lambda x: {
		'color' : x['properties']['fill'],
		'opacity': x['properties']['opacity'],
		'fillOpacity':  x['properties']['opacity'],
		'fillColor' : x['properties']['fill'],
		}).add_to(Hmap)

	Hmap.save("./Html/"+date+".html")

def CreateHtmlSelectedNodes(data,opacity=0.4):
	Hmap = folium.Map(location=[46.066666, 11.116667], zoom_start=10)
	folium.GeoJson(data, name="Trento").add_to(Hmap)
	folium.GeoJson(data, name="Trento",   style_function=lambda x: {
		'color' : x['properties']['fill'],
		'opacity': x['properties']['opacity'],
		'fillOpacity':  x['properties']['opacity'],
		'fillColor' : x['properties']['fill'],
		}).add_to(Hmap)

	Hmap.save("./Selected.html")
	CreateImage("Selected",file_location="/home/ali-jawad/Desktop/hona-sim/")	

################### IMAGES #######################

def CreateImage(date,file_location="/home/ali-jawad/Desktop/data1/geojson/Html"):
	options = webdriver.ChromeOptions()
	WINDOW_SIZE = "1920,1080"
	#options.add_argument("--start-maximized")
	options.add_argument("--headless")
	options.add_argument("--window-size=%s" % WINDOW_SIZE)
	driver = webdriver.Chrome(chrome_options=options)
	driver.get("file:///home/ali-jawad/Desktop/hona-sim/Selected.html")
	driver.save_screenshot("./Images/"+date+".png")
	driver.quit()


def AddText(date,color='rgb(0, 0, 0)',size=80,position=(20,20)): 
	font = ImageFont.truetype('impact.ttf', size=size)
	img = Image.open("./Images/"+date+".png")
	draw = ImageDraw.Draw(img)
	# font = ImageFont.truetype(<font-file>, <font-size>)
	#font = ImageFont.truetype("sans-serif.ttf", 16)
	# draw.text((x, y),"Sample Text",(r,g,b))
	draw.text(position,ReturnDateName(date),color,font=font)
	img.save("./Images/out_"+date+".png")

def ReturnDateName(date):
	newformat=date.split("-")[2]+" "+date.split("-")[1]+" "+date.split("-")[0]
	formated = datetime.datetime.strptime(newformat, '%d %m %Y').weekday()
	return {
	"Monday":"Mon",
	"Tuesday":"Tue",
	"Wednesday":"Wen",
	"Thursday":"Thu",
	"Friday":"Fri",
	"Saturday":"Sat",
	"Sunday":"Sun"	
	}[calendar.day_name[formated]]

################### GIFS #########################
def CreateGif(dates): 
	with imageio.get_writer("./GIFS/OneWeek.gif", mode='I',duration=2) as writer:
		for date in dates:
			image = imageio.imread("./Images/out_"+date+".png")
			writer.append_data(image)

################## Hour ##########################

def HGetdata(date): 
	df=sort.LoadPickle("../RBSHour_trento/RBSHour_Trento_"+date+".pk")
	return df

def HCreateHeatMap(date,x=51,y=44):
	df  =HGetdata(date)
	IDList=HighlightTrento(x=51,y=44)
	Max,Min = HGetMaxMin(df)

	for i in range(24):
		newcells=[]
		data=LoadGeoJson("./trentino-grid.geojson") 
		Cells=data['features']
		for cell in Cells: 
			if cell["properties"]["cellId"] in IDList: 
				newcells.append(cell)
		data['features']=newcells
		data=HAssignColors(data,df,Max,Min,str(i))
		DumpGeoJson("./HHeatMap_Trento/Trento_"+date+"_"+str(i)+".geojson",data)

def HAssignColors(data,df,Max,Min,c): 
	# Adding the new colours to the GeoJson Code
	Cells=data['features']
	newcells=[]
	for cell in Cells:
		ID=cell["properties"]["cellId"] 
		if ID in df.index: 
			Req=df.at[ID,c]
			cell["properties"]["#Req"]=str(Req)
			cell["properties"]["fill"]=HGetColor(Req,Max,Min)
			newcells.append(cell)
	data['features']=newcells
	return data 

def HGetColor(Req,Max,Min):
	# Give a colour value in HEX according to 
	# the number of request received by the square
	map={1:"#ffffcc",
	2:"#ffeda0",
	3:"#fed976",
	4:"#feb24c",
	5:"#fd8d3c",
	6:"#fc4e2a",
	7:"#e31a1c",
	8:"#bd0026",
	9:"#800026"}

	return map[floor(9*(Req-Min)/(Max-Min+2))+1]

def HGetMaxMin(df): 
	Max=df.iat[0,0]
	Min=df.iat[0,0]
	for c in df.columns: 
		for ID in df.index:
			test=df.at[ID,c] 
			if test > Max: 
				Max = test
			if test < Min: 
				Min = test

	return Max,Min


def HCreateHtml(date,opacity=0.6):
	for i in range(24):
		print(date+"_"+str(i))
		data=LoadGeoJson("./HHeatMap_Trento/Trento_"+date+"_"+str(i)+".geojson")
		Hmap = folium.Map(location=[46.066666, 11.116667], zoom_start=12)
		folium.GeoJson(data, name="Trento").add_to(Hmap)
		folium.GeoJson(data, name="Trento",   style_function=lambda x: {
		'color' : x['properties']['fill'],
		'opacity': opacity,
		'fillOpacity': opacity,
		'fillColor' : x['properties']['fill'],
		}).add_to(Hmap)

		Hmap.save("./HHtml/"+date+"_"+str(i)+".html")

def HCreateImage(date):
	file_location="/home/ali-jawad/Desktop/data1/geojson/HHtml"
	glob_folder = os.path.join(file_location, '*.html')

	html_file_list = glob.glob(glob_folder)

	for html_file in html_file_list:
		if date not in html_file: 
			continue
		I=html_file.split(date+"_")[1].split(".")[0]
		print(I)
		#get the name into the right format
		temp_name = "file://" + html_file
		# open in webpage
		driver = webdriver.Chrome()
		driver.get(temp_name)      
		driver.save_screenshot("./Images/"+date+".png")
		driver.quit()

		# crop as required
		img = Image.open("./Images/"+date+".png",mode='r')
		width, height = img.size
		hcrop=800
		vcrop=100

		box = (int(hcrop/2), int(vcrop/2), width - int(hcrop/2), height -int(vcrop/2)-50)
		area = img.crop(box)
		area.save("./HImages/"+date+"_"+I+".png", 'png')

def HAddText(date,color='rgb(0, 0, 0)',size=80,position=(20,20)):
	for i in range(24):
		font = ImageFont.truetype('impact.ttf', size=size)
		img = Image.open("./HImages/"+date+"_"+str(i)+".png")
		draw = ImageDraw.Draw(img)
		# font = ImageFont.truetype(<font-file>, <font-size>)
		#font = ImageFont.truetype("sans-serif.ttf", 16)
		# draw.text((x, y),"Sample Text",(r,g,b))
		draw.text(position,str(i)+"H",color,font=font)
		img.save("./HImages/out_"+date+"_"+str(i)+".png")

def HCreateGif(date): 
	with imageio.get_writer("./HGIFS/OneDay_"+date+".gif", mode='I',duration=2) as writer:
		for i in range(24):
			image = imageio.imread("./HImages/out_"+date+"_"+str(i)+".png")
			writer.append_data(image)