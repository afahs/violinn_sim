#!/usr/bin/python3
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd 
import organize 
import random 

def LoadPickle(dir1): 
	return pd.read_pickle(dir1)

def ChooseBS(df,bs,load):
	resdf=df[df["BS"].isin([bs])]
	resdf=resdf[["BS", "Time", load]]
	return resdf

def BinReq(df):
	for i in range(len(df.values)):
		if df.iat[i,2]!= "N/A":
			df.iat[i,2]=0
		else : df.iat[i,2]=1
	return df

def CountDfMin(df):
	newdf=pd.DataFrame(columns=["Time","freq"])
	Time=0
	summ=0
	for i in range(144):
		summ=0
		for j in range(len(df.values)): 
			if df.iat[j,1]==i:
				summ+=1
		newdf.loc[i]=[i,summ]
	return newdf

def CountDfHour(df):
	Time=0
	summ=0
	hourdf=pd.DataFrame(columns=["Time","freq"])
	for i in range(144):
		if (i+1)%6==0:
			summ+=df.at[i,"freq"] 
			hourdf.loc[Time]=[Time,summ]
			summ=0
			Time+=1
		else:
			summ+=df.at[i,"freq"]
	return hourdf

def CountDf(df,bs,load): # the function that returns the number of requests 
# each hour and each 10 mins for a single bas station
	resdf=ChooseBS(df,bs,load)
	resdf=BinReq(resdf)	
	resdf=resdf[resdf[load].isin([1])]
	mindf=CountDfMin(resdf)
	hourdf=CountDfHour(mindf)
	return mindf,hourdf

def OrganizeDf(df,load): 
	newdf=df[["BS",load]]
	#newdf=newdf[newdf["BS"].isin(range(400))]
	newdf=newdf[~newdf[load].isin(["N/A"])]
	return newdf

def ReqPerBs(df):
	# a function that will return the number of requests received by the bs 
	# in a full day
	l={}
	for i in range(len(df.values)):
		if df.iat[i,0] not in list(l.keys()): 
			l[df.iat[i,0]]=1
		else: 
			l[df.iat[i,0]]+=1 
	newdf = pd.DataFrame(l.items(),columns=["BS","#Req"])
	newdf=newdf.sort_values(by=["BS"])
	s=pd.Series(range(len(newdf.values)))
	newdf=newdf.set_index(s)
	return newdf 

def GetRandomBS(df,n): 
	l=df.BS.unique().tolist()
	RandomBs=random.choices(l, k=n)
	print(RandomBs)
	return RandomBs

def CreateMap(Nodes,LBS): 
	mapNode={}
	i=0
	for n in Nodes:
		mapNode[n]=str(LBS[i])
		i+=1
	return mapNode
def GetID(x,y): 
	return 117*y+x+1

def GetSymPoint(x,y,XG,YG): 
	nx=2*XG-x
	ny=2*YG-y
	return nx,ny

def GetDistributedBS(df,n,ns,XG=51,YG=44,s=40):
	l=df.BS.unique().tolist()
	BSlist=[]
	# Dedicated nodes
	if n%2==1: 
		BSlist.append(GetID(XG,YG))
		n-=1
	for i in range(int(n/2)):
		x=random.randint(XG-s+1,XG+s)
		y=random.randint(YG-s+1,YG+s)
		nx,ny=GetSymPoint(x,y,XG,YG)
		while GetID(x,y) in BSlist or (x==XG and y==YG) or (GetID(x,y) not in l) or (GetID(nx,ny) not in l): 
			x=random.randint(XG-s+1,XG+s)
			y=random.randint(YG-s+1,YG+s)
			nx,ny=GetSymPoint(x,y,XG,YG)
		BSlist.append(GetID(x,y))
		BSlist.append(GetID(nx,ny))
	# Spare nodes should be colocated with a random dedicated node
	colocate=random.sample(range(n),ns)
	for i in range(ns):
		BSlist.append(BSlist[colocate[i]])	
	return BSlist


def HighlightTrento(x=51,y=44): 
	IDList=[]
	StartX=x
	EndX=x+20
	StartY=y-19
	EndY=y+20
	for i in range(StartX,EndX+1): 
		for j in range(StartY,EndY+1):
			IDList.append(117*j+i+1)
	return(IDList)


def GetLoad(df,Nodes,load,mapNode,BSlist):

	names=["Time"]
	for n in Nodes: 
		names.append(str(n))
	newdf = pd.DataFrame(columns=names)
	newdfmin = pd.DataFrame(columns=names)
	dfsmin=[]
	dfshour=[]
	for bs in BSlist:
		mindf,hourdf=CountDf(df,bs,load)
		dfsmin.append(mindf)
		dfshour.append(hourdf)
	for i in range(len(dfshour[0].values)):
		temp=[i]
		for j in range(len(BSlist)):
			temp.append(dfshour[j].at[i,"freq"])
		newdf.loc[i]=temp

	for i in range(len(dfsmin[0].values)):
		temp=[i]
		for j in range(len(BSlist)):
			temp.append(dfsmin[j].at[i,"freq"])
		newdfmin.loc[i]=temp

	return newdf,newdfmin,mapNode


if __name__ == "__main__":
	
	load="Internet"
	bs=40
	date="2013-11-25"
	df=LoadPickle("./Pk/"+date+".pk")
	print(df)
	#dfhour,dfmin=GetLoad(df,5,load)
	#print(dfhour)
	#['BS', 'Time', 'CC', 'SMSIn', 'SMSOut', 'CallOut', 'CallIn', 'Internet']
	# mindf,hourdf=CountDf(df,bs,load)
	# print(hourdf)
	# hourdf.plot.bar(x="Time",y="freq")
	# plt.show()
	#newdf=OrganizeDf(df,load)
	#newdf=ReqPerBs(newdf)
	#print(newdf)

	#organze.SaveDF(newdf,"./LoadPerBS/LoadPerBS_"+date+".csv")
	#organze.PickleDF(newdf,"./LoadPerBS/LoadPerBS_"+date+".pk")


