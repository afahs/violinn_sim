#!/usr/bin/python3
import matplotlib.pyplot as plt
from matplotlib import animation
import math 
import numpy as np
import pandas as pd 
import organize 
import random 
import com
import argparse
import sort
import time
import pickle
import late
import animate as anime
import voila

def parseCliOptions():

		parser = argparse.ArgumentParser()

		parser.add_argument( '--Cycle',#imp
			dest       = 'Cycle',  
			type       = float,
			default    = 10,
			help       = 'cycle time in seconds',
		)

		parser.add_argument( '--Date', #imp
			dest       = 'Date', 
			type       = str,
			default    = '2013-11-25',
			help       = 'The date of the used data',
		)

		parser.add_argument( '--Load', #imp
			dest       = 'Load', 
			type       = str,
			default    = 'Internet',
			help       = 'The tested type of load',
		)

		parser.add_argument( '--Random', #imp
			dest       = 'Random', 
			action	   ='store_true',
			help       = 'The BS are selected randomly',
		)

		parser.add_argument( '--Animate', #imp
			dest       = 'Animate', 
			action     ='store_true',
			help       = 'Animate the Load distribution',

		)

		parser.add_argument( '--Svc', #imp
			dest       = 'Svc', 
			type       = str,
			default    = "0.0.0.0",
			help       = 'The tested type of load',
		)


		options        = parser.parse_args()
		return options.__dict__
def PickleLoad(filename):
	"""
	Load the saved pk files
	"""
	pickledlist=[]
	with open("./"+filename, 'rb') as f:
		pickledlist = pickle.load(f)
	return pickledlist

def PrintRow(Row):
	s=""
	for item in Row: 
		s+=str(item)+"\t"
	print(s)

def Distribute(dfhour,cycle,mapNode,Animate,Svc):
	newdf=pd.DataFrame(columns=["Time"]+ list(mapNode.keys()) )
	newdf.set_index('Time', inplace=True)
	newdf.to_pickle("./temp.pk")
	if Animate: 
		com.RunGraphs()
		time.sleep(2)
	#dfhour.set_index('Time', inplace=True)
	PrintRow(["Time"]+ list(mapNode.keys()))
	for i, row in dfhour.iterrows():
		PrintRow(list(row))
		newdf.loc[i]=row
		newdf.to_pickle("./temp.pk")
		for j in range(newdf.loc[i].max()): 
			for node in list(newdf.columns):
				if newdf.at[i,node]>j:
					com.SendReq(node,Svc)
		time.sleep(cycle)


if __name__ == "__main__":
	options = parseCliOptions()
	date=options["Date"]
	load=options["Load"]
	cycle=options["Cycle"]
	animate=options["Animate"]
	svc=options["Svc"]
	df=sort.LoadPickle("./Pk/"+date+".pk")
	dfhour,dfmin,mapNode=sort.GetLoad(df,GetNodes(),load)
	late.main(mapNode)
	anime.MapNodes(mapNode)
	pickle_out = open("mapNode.pk","wb")
	pickle.dump(mapNode, pickle_out)
	pickle_out.close()
	input("Press Enter to continue...")
	Distribute(dfhour,cycle,mapNode,animate,svc)

def GetDistribution(Nodes,NewLoad=False,date='2013-11-30',load='Internet',cycle=1,animate=True,days=1,nodeObjects=[], SpecificLoad=False):
	if NewLoad==True: 
		dfhour,dfmin,mapNode,Latencies=CreateDf(Nodes,days,nodeObjects,date=date,load=load)#,loadType='original'
		anime.MapNodes(mapNode,nodeObjects)
	else:
		dfhour=PickleLoad("temp/dfhour.pk")
		dfmin=PickleLoad("temp/dfmin.pk")
		mapNode=PickleLoad("temp/mapNode.pk")
		Latencies=PickleLoad("temp/lat.pk")
	return dfhour,dfmin,mapNode,Latencies

def GetLatencies(Nodes,LatencyDF):
	"""
	Calculates the Latency between the nodes using the geographical distance between them
	"""
	latencies={}
	for node in Nodes:
		latencies[node]=[]
		for n in Nodes: 
			dist=LatencyDF.loc[node,n]
			#Spare nodes are colocated but their latency is assumed to be 0.5ms
			if dist==0:
				dist=0.5
			latencies[node].append((n,float(format(dist,'.2f'))))
	return latencies

# def CreateDf(Nodes,days,nodeObjects,date='2013-11-30',load='Internet', loadType='original'): 
# 	if loadType == 'original':
# 		df=sort.LoadPickle("./LoadData/Pk/"+date+".pk")
# 		if days==2: 
# 			dates=date.split("-")
# 			dates[2]=str(int(dates[2])+1)
# 			date=dates[0]+"-"+dates[1]+"-"+dates[2]
# 			df2=sort.LoadPickle("./LoadData/Pk/"+date+".pk")
# 		numberSpareNodes= len(voila.GetNodesOfType("S",nodeObjects))
# 		BSlist=sort.GetDistributedBS(df,len(Nodes),numberSpareNodes,XG=51,YG=44,s=40)
# 		print(BSlist)
# 		mapNode=sort.CreateMap(Nodes,BSlist)
# 		print(Nodes)
# 		dfhour,dfmin,mapNode=sort.GetLoad(df,Nodes,load,mapNode,BSlist)
# 		if days==2: 
# 			dfhour2,dfmin2,mapNode2=sort.GetLoad(df,Nodes,load,mapNode,BSlist)

# 		LatencyDF=late.GetLatencyMap(mapNode)
# 		Latencies=GetLatencies(Nodes,LatencyDF)

# 		if days==2: 
# 			dfhour=pd.concat([dfhour, dfhour2]).reset_index(drop=True)
# 		dfhour.to_pickle("./temp/dfhour.pk")
# 		dfmin.to_pickle("./temp/dfmin.pk")
# 		with open("./temp/mapNode.pk", 'wb') as f:
# 			pickle.dump(mapNode, f, pickle.HIGHEST_PROTOCOL)
# 		with open('./temp/lat.pk', 'wb') as f:
# 			pickle.dump(Latencies, f, pickle.HIGHEST_PROTOCOL)
# 	else:
# 		dfhour,dfmin,mapNode,Latencies=CreateDfORCH(Nodes, nodeObjects,days, date, load)
# 	return dfhour,dfmin,mapNode,Latencies
	
# def CreateDfORCH(Nodes,nodeObjects,days,date,load): 
# 	#Get the full dataset for a date
# 	df=sort.LoadPickle("./LoadData/Pk/"+date+".pk")
# 	#Special handling to get the next day if needed
# 	if days==2: 
# 		dates=date.split("-")
# 		dates[2]=str(int(dates[2])+1)
# 		date=dates[0]+"-"+dates[1]+"-"+dates[2]
# 		df2=sort.LoadPickle("./LoadData/Pk/"+date+".pk")
# 	#Get the BS list.
# 	#Only dedicated nodes should be associated to an actual BS
# 	numberDedicatedNodes= len(voila.GetNodesOfType("D",nodeObjects))
# 	numberSpareNodes=len(Nodes)-numberDedicatedNodes
# 	#Currently the nodes are mirrored wrt Trento city. 
# 	#This function will also colocate spare nodes with a dedicated node
# 	BSlist=sort.GetDistributedBS(df,numberDedicatedNodes,numberSpareNodes,XG=51,YG=44,s=40)
# 	print(BSlist)
# 	mapNode=sort.CreateMap(Nodes,BSlist)
# 	print(Nodes)
# 	#For now spare nodes get a load first, but they are Idle so they are removed as gateways
# 	dfhour,dfmin,mapNode=sort.GetLoad(df,Nodes,load,mapNode,BSlist)
# 	if days==2: 
# 		dfhour2,dfmin2,mapNode2=sort.GetLoad(df,Nodes,load,mapNode,BSlist)
# 	LatencyDF=late.GetLatencyMap(mapNode)
# 	Latencies=GetLatencies(Nodes,LatencyDF)
# 	if days==2: 
# 		dfhour=pd.concat([dfhour, dfhour2]).reset_index(drop=True)
# 	dfhour.to_pickle("./temp/dfhour.pk")
# 	dfmin.to_pickle("./temp/dfmin.pk")
# 	with open("./temp/mapNode.pk", 'wb') as f:
# 		pickle.dump(mapNode, f, pickle.HIGHEST_PROTOCOL)
# 	with open('./temp/lat.pk', 'wb') as f:
# 		pickle.dump(Latencies, f, pickle.HIGHEST_PROTOCOL)
# 	return dfhour,dfmin,mapNode,Latencies

def CreateDf(Nodes,days,nodeObjects,date='2013-11-30',load='Internet'): 
	df=sort.LoadPickle("./LoadData/Pk/"+date+".pk")
	if days==2: 
		dates=date.split("-")
		dates[2]=str(int(dates[2])+1)
		date=dates[0]+"-"+dates[1]+"-"+dates[2]
		df2=sort.LoadPickle("./LoadData/Pk/"+date+".pk")
	numberSpareNodes= len(voila.GetNodesOfType("S",nodeObjects))
	BSlist=sort.GetDistributedBS(df,len(Nodes)-numberSpareNodes,numberSpareNodes,XG=51,YG=44,s=40)
	print(BSlist)
	mapNode=sort.CreateMap(Nodes,BSlist)
	print(Nodes)
	dfhour,dfmin,mapNode=sort.GetLoad(df,Nodes,load,mapNode,BSlist)
	if days==2: 
		dfhour2,dfmin2,mapNode2=sort.GetLoad(df,Nodes,load,mapNode,BSlist)

	LatencyDF=late.GetLatencyMap(mapNode)
	Latencies=GetLatencies(Nodes,LatencyDF)

	if days==2: 
		dfhour=pd.concat([dfhour, dfhour2]).reset_index(drop=True)
	dfhour.to_pickle("./temp/dfhour.pk")
	dfmin.to_pickle("./temp/dfmin.pk")
	with open("./temp/mapNode.pk", 'wb') as f:
		pickle.dump(mapNode, f, pickle.HIGHEST_PROTOCOL)
	with open('./temp/lat.pk', 'wb') as f:
		pickle.dump(Latencies, f, pickle.HIGHEST_PROTOCOL)

	return dfhour,dfmin,mapNode,Latencies
