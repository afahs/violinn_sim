#Frigo4

IF=eth0
tc qdisc add dev $IF root handle 1: htb
tc class add dev $IF parent 1: classid 1:1 htb rate 1000Mbps


#Frigo1

tc class add dev $IF parent 1:1 classid 1:2 htb rate 1000Mbps
tc qdisc add dev $IF handle 2: parent 1:2 netem delay 14.53ms
tc filter add dev $IF pref 2 protocol ip u32 match ip dst 131.254.150.180 flowid 1:2

#Frigo2

tc class add dev $IF parent 1:1 classid 1:3 htb rate 1000Mbps
tc qdisc add dev $IF handle 3: parent 1:3 netem delay 7.81ms
tc filter add dev $IF pref 3 protocol ip u32 match ip dst 131.254.150.181 flowid 1:3

#Frigo3

tc class add dev $IF parent 1:1 classid 1:4 htb rate 1000Mbps
tc qdisc add dev $IF handle 4: parent 1:4 netem delay 25.3ms
tc filter add dev $IF pref 4 protocol ip u32 match ip dst 131.254.150.182 flowid 1:4

#Frigo5

tc class add dev $IF parent 1:1 classid 1:5 htb rate 1000Mbps
tc qdisc add dev $IF handle 5: parent 1:5 netem delay 29.07ms
tc filter add dev $IF pref 5 protocol ip u32 match ip dst 131.254.150.184 flowid 1:5

