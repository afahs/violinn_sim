#Node5

IF=eth0
tc qdisc add dev $IF root handle 1: htb
tc class add dev $IF parent 1: classid 1:1 htb rate 1000Mbps


#Frigo1

tc class add dev $IF parent 1:1 classid 1:2 htb rate 1000Mbps
tc qdisc add dev $IF handle 2: parent 1:2 netem delay 4.0ms
tc filter add dev $IF pref 2 protocol ip u32 match ip dst 131.254.150.180 flowid 1:2

#Frigo2

tc class add dev $IF parent 1:1 classid 1:3 htb rate 1000Mbps
tc qdisc add dev $IF handle 3: parent 1:3 netem delay 16.99ms
tc filter add dev $IF pref 3 protocol ip u32 match ip dst 131.254.150.181 flowid 1:3

#Frigo3

tc class add dev $IF parent 1:1 classid 1:4 htb rate 1000Mbps
tc qdisc add dev $IF handle 4: parent 1:4 netem delay 12.98ms
tc filter add dev $IF pref 4 protocol ip u32 match ip dst 131.254.150.182 flowid 1:4

#Frigo4

tc class add dev $IF parent 1:1 classid 1:5 htb rate 1000Mbps
tc qdisc add dev $IF handle 5: parent 1:5 netem delay 14.32ms
tc filter add dev $IF pref 5 protocol ip u32 match ip dst 131.254.150.183 flowid 1:5

#Frigo5

tc class add dev $IF parent 1:1 classid 1:6 htb rate 1000Mbps
tc qdisc add dev $IF handle 6: parent 1:6 netem delay 22.2ms
tc filter add dev $IF pref 6 protocol ip u32 match ip dst 131.254.150.184 flowid 1:6

#Node2

tc class add dev $IF parent 1:1 classid 1:7 htb rate 1000Mbps
tc qdisc add dev $IF handle 7: parent 1:7 netem delay 15.79ms
tc filter add dev $IF pref 7 protocol ip u32 match ip dst 131.254.150.186 flowid 1:7

#Node3

tc class add dev $IF parent 1:1 classid 1:8 htb rate 1000Mbps
tc qdisc add dev $IF handle 8: parent 1:8 netem delay 16.29ms
tc filter add dev $IF pref 8 protocol ip u32 match ip dst 131.254.150.187 flowid 1:8

#Node4

tc class add dev $IF parent 1:1 classid 1:9 htb rate 1000Mbps
tc qdisc add dev $IF handle 9: parent 1:9 netem delay 8.0ms
tc filter add dev $IF pref 9 protocol ip u32 match ip dst 131.254.150.188 flowid 1:9

#Node6

tc class add dev $IF parent 1:1 classid 1:10 htb rate 1000Mbps
tc qdisc add dev $IF handle 10: parent 1:10 netem delay 22.5ms
tc filter add dev $IF pref 10 protocol ip u32 match ip dst 131.254.150.190 flowid 1:10

#Node7

tc class add dev $IF parent 1:1 classid 1:11 htb rate 1000Mbps
tc qdisc add dev $IF handle 11: parent 1:11 netem delay 16.8ms
tc filter add dev $IF pref 11 protocol ip u32 match ip dst 131.254.150.191 flowid 1:11

#Frigo1-1

tc class add dev $IF parent 1:1 classid 1:12 htb rate 1000Mbps
tc qdisc add dev $IF handle 12: parent 1:12 netem delay 8.63ms
tc filter add dev $IF pref 12 protocol ip u32 match ip dst 131.254.150.140 flowid 1:12

#Frigo1-2

tc class add dev $IF parent 1:1 classid 1:13 htb rate 1000Mbps
tc qdisc add dev $IF handle 13: parent 1:13 netem delay 1.58ms
tc filter add dev $IF pref 13 protocol ip u32 match ip dst 131.254.150.141 flowid 1:13

#Frigo1-3

tc class add dev $IF parent 1:1 classid 1:14 htb rate 1000Mbps
tc qdisc add dev $IF handle 14: parent 1:14 netem delay 15.81ms
tc filter add dev $IF pref 14 protocol ip u32 match ip dst 131.254.150.142 flowid 1:14

#Frigo1-4

tc class add dev $IF parent 1:1 classid 1:15 htb rate 1000Mbps
tc qdisc add dev $IF handle 15: parent 1:15 netem delay 23.54ms
tc filter add dev $IF pref 15 protocol ip u32 match ip dst 131.254.150.143 flowid 1:15

#Frigo1-5

tc class add dev $IF parent 1:1 classid 1:16 htb rate 1000Mbps
tc qdisc add dev $IF handle 16: parent 1:16 netem delay 23.9ms
tc filter add dev $IF pref 16 protocol ip u32 match ip dst 131.254.150.144 flowid 1:16

#Frigo2-1

tc class add dev $IF parent 1:1 classid 1:17 htb rate 1000Mbps
tc qdisc add dev $IF handle 17: parent 1:17 netem delay 16.35ms
tc filter add dev $IF pref 17 protocol ip u32 match ip dst 131.254.150.145 flowid 1:17

#Frigo2-2

tc class add dev $IF parent 1:1 classid 1:18 htb rate 1000Mbps
tc qdisc add dev $IF handle 18: parent 1:18 netem delay 22.55ms
tc filter add dev $IF pref 18 protocol ip u32 match ip dst 131.254.150.146 flowid 1:18

#Frigo2-3

tc class add dev $IF parent 1:1 classid 1:19 htb rate 1000Mbps
tc qdisc add dev $IF handle 19: parent 1:19 netem delay 14.58ms
tc filter add dev $IF pref 19 protocol ip u32 match ip dst 131.254.150.147 flowid 1:19

#Frigo2-4

tc class add dev $IF parent 1:1 classid 1:20 htb rate 1000Mbps
tc qdisc add dev $IF handle 20: parent 1:20 netem delay 4.61ms
tc filter add dev $IF pref 20 protocol ip u32 match ip dst 131.254.150.148 flowid 1:20

#Frigo2-5

tc class add dev $IF parent 1:1 classid 1:21 htb rate 1000Mbps
tc qdisc add dev $IF handle 21: parent 1:21 netem delay 11.88ms
tc filter add dev $IF pref 21 protocol ip u32 match ip dst 131.254.150.149 flowid 1:21

