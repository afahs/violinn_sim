#!/usr/bin/python3

################## STD LIBS #################
import pickle as pk
import time
import random
import pandas
from statistics import mean
################## LIBS #####################
import hona
import voila
def WriteCsvRow(f,row):
	str1=""
	size=len(row)
	for i in range(size): 
		if i ==size-1: 
			str1+=str(row[i])+"\n"
		else:
			str1+=str(row[i])+","
	f.write(str1)


def PickleNodes(NodeObjects,latencies):
	"""
	save the new network and load configuration
	"""
	with open('./node.pk', 'wb') as f:
		pk.dump(NodeObjects,f, pk.HIGHEST_PROTOCOL)

	with open('./lat.pk', 'wb') as f:
		pk.dump(latencies,f, pk.HIGHEST_PROTOCOL)

def LoadValue(mean=300,Variance=100): 
	return abs(int(random.normalvariate(mean, Variance)))

def GetSortedNodes(NodeObjects):
	"""
	Returns a list of Sorted Nodes
	"""
	Nodes=[]
	for n in NodeObjects:
		Nodes.append(n.name)
	Nodes=sorted(Nodes)
	return Nodes

def UpdateLoad(NodeObjects,dfhour,cycle,cum=False):
	#print(dfhour)
	for node in NodeObjects:
		if not node.Idle:
			if cum: 
				node.req= node.req+dfhour.loc[cycle,node.name]  
			else: 
				node.req=dfhour.loc[cycle,node.name]
	with open('./temp/node.pk', 'wb') as f:
		pk.dump(NodeObjects,f, pk.HIGHEST_PROTOCOL)
	return NodeObjects

def UpdateBS(NodeObjects,mapNode):
	#print(dfhour)
	for node in NodeObjects:
		node.BS = mapNode[node.name]
	return NodeObjects

def GetColocatedNodes(mapNode):
	NodePerBS={}
	Bs=list(mapNode.values())
	for B in Bs: 
		NodePerBS[B]=[]

	for k,v in mapNode.items():
		NodePerBS[v].append(k)
	return NodePerBS


def printLoadPerNode(NodeObjects): 
	for node in NodeObjects: 
		print(node.name,node.req,end="|")
	print("")

def Idlenodes(NodeObjects): 
	n=0
	for node in NodeObjects: 
		if node.req==0: 
			n+=1
	return n
def CalculateActualEP(tolerance,SaturationPerPod,Actual,TempCase,TotalReq):
	AVc0=0
	for P in TempCase.RPP: 
		if P> Actual: 
			AVc0+=(P-Actual)
	if TotalReq!=0: 
		AEp=(TempCase.Vl0 + AVc0)*100/TotalReq
	else: 
		AEp=0
	print(AEp)
	return AEp,AVc0

def Check2(cycle,NodeObjects,LPC,dfhour,dfmin,mapNode,latencies,SelectedNodes,L,Ep0,f,g,Tol,Pr,Cs,Ps,Ca,fitnessAlpha,algoAlt,VProximity=99.9, VImbalance=5,SaturationPerPod=50,Timeout=1,sleep=1): 
	print("######################## Cycle %i #######################" % cycle)
	tolerance=Tol #[0,1]
	#print(LPC)
	Actual=SaturationPerPod #The real saturation of the pod
	SaturationPerPod=SaturationPerPod*(1-tolerance) # The passed saturation for provisioning
	NodeObjects=UpdateLoad(NodeObjects,dfhour,cycle,cum=False)
	Nodes=GetSortedNodes(NodeObjects)
	LatMat=hona.GetLatMat(latencies,Nodes)
	ProbMat=hona.GetProbMat(latencies,Nodes,B=1,index=1)#put in Main
	TestMat=hona.CreateTestMat(LatMat,L)
	ListReq=LPC[cycle]
	TotalReq=sum(ListReq)
	print(TotalReq)
	#ListReq,TotalReq=hona.CreateListReq(Nodes,NodeObjects)
	#printLoadPerNode(NodeObjects)
	#print(ListReq,TotalReq,Idlenodes(NodeObjects))
	# NodePerBS=GetColocatedNodes(mapNode)
	# for BS,NodeList in NodePerBS.items():
	# 	if len(NodeList>1): 
	# 		print(BS,NodeList)
	# 		assert False
	# 	assert len(NodeList)<3, "Three colocated Nodes"

	TempCase=hona.CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,SaturationPerPod)
	print(TempCase.RPP)
	AEp,AVc0=CalculateActualEP(tolerance,SaturationPerPod,Actual,TempCase,TotalReq)

	print("Ep:",TempCase.Ep,"Vl0:",TempCase.Vl0,"Vc0",TempCase.Vc0,"MaxLPP:",TempCase.MaxLPP,"Total:",sum(TempCase.RPP),[Nodes.index(node) for node in SelectedNodes])
	Cost=voila.CalculateCost(TempCase,TempCase,NodeObjects,Cs,Ca)
	NumberS=voila.ReturnNumberS(TempCase.Nodes,NodeObjects)
	row=[len(TempCase.Nodes),TempCase.Proximity,TempCase.Imbalance,sum(TempCase.RPP),TempCase.MaxLPP,sum(TempCase.RPP)*100/(len(TempCase.Nodes)*SaturationPerPod),Cost,NumberS]
	if TempCase.Vc0>0.5: 
		print("Actual",Actual,"Passed",SaturationPerPod,"Actual E",AEp, "Calculated Ep",TempCase.Ep)
	newdf=pandas.read_pickle("./temp.pk")
	newdf.loc[cycle]=row
	newdf.to_pickle("./temp.pk")
	TotalLostCycle=TotalReq*TempCase.Ep/100
	#hona.CheckForReplacement(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L)
	#print(ListReq)
	#print("DF value",dfhour.sum(axis=1)[cycle])
	Violation="None"
	Studied=0
	Change="None"
	#print(Changes)
	Best=TempCase
	
	#Latency violation
	assert algoAlt in ["IgnoreS","QoSOnly","QoSMinCost","QoSMinCostImp","QoSFitness"]," Wrong value of algoAlt was passed: \"%s\"" % algoAlt
	if Ep0<TempCase.Ep and TempCase.Vl0 > TempCase.Vc0:
		start=time.time()
		Violation="Proximity"
		#Studied=nc
		#Switch according to algorithm chosen for evaluation
		if algoAlt=="IgnoreS": 
			Best=voila.FixProximityIgnoreS(Nodes,NodeObjects,SelectedNodes,start,
									TempCase,TestMat,
									ProbMat,LatMat,ListReq,
									TotalReq,L,VProximity,Ep0,Cs,Ca,
									VImbalance,SaturationPerPod,Timeout)
		if algoAlt=="QoSOnly":	
			Best=voila.FixProximityQoS(Nodes,NodeObjects,SelectedNodes,start,
									TempCase,TestMat,
									ProbMat,LatMat,ListReq,
									TotalReq,L,VProximity,Ep0,Cs,Ca,
									VImbalance,SaturationPerPod,Timeout)
		if algoAlt=="QoSMinCost":	
			Best=voila.FixProximityQoSMinCost(Nodes,NodeObjects,SelectedNodes,start,
									TempCase,TestMat,
									ProbMat,LatMat,ListReq,
									TotalReq,L,VProximity,Ep0,Cs,Ca,
									VImbalance,SaturationPerPod,Timeout)
		if algoAlt=="QoSMinCostImp":	
			#TODO: GitlabIssue #6
			print("FixProximity is not yet implemented for algorithm alternative QoSMinCostImp")
		if algoAlt=="QoSFitness":	
			Best=voila.FixProximityQoSFitness(Nodes,NodeObjects,SelectedNodes,start,
									TempCase,TestMat,
									ProbMat,LatMat,ListReq,
									TotalReq,L,VProximity,Ep0,Cs,Ca,fitnessAlpha,
									VImbalance,SaturationPerPod,Timeout)
		
		if Best.Ep<Ep0: 
			#print("A replacement is Done")
			time.sleep(sleep)
		else:
			print("No Solution was found")

	#Overload violation 
	if Ep0<TempCase.Ep and TempCase.Vl0 < TempCase.Vc0:
		Violation="Saturation"
		#Studied=nc	
		start=time.time()

		#Switch according to algorithm chosen for evaluation
		if algoAlt=="IgnoreS": 
			Best=voila.FixSaturationIgnoreS(Nodes,NodeObjects,SelectedNodes,start,
									TempCase,TestMat,
									ProbMat,LatMat,ListReq,
									TotalReq,L,VProximity,Ep0,Cs,Ca,
									VImbalance,SaturationPerPod,Timeout)
		if algoAlt=="QoSOnly":	
			Best=voila.FixSaturationQoS(Nodes,NodeObjects,SelectedNodes,start,
								TempCase,TestMat,
								ProbMat,LatMat,ListReq,
								TotalReq,L,VProximity,Ep0,Cs,Ca,
								VImbalance,SaturationPerPod,Timeout,number=0)
		if algoAlt=="QoSMinCost":	
			Best=voila.FixSaturationQoSMinCost(Nodes,NodeObjects,SelectedNodes,start,
								TempCase,TestMat,
								ProbMat,LatMat,ListReq,
								TotalReq,L,VProximity,Ep0,Cs,Ca,
								VImbalance,SaturationPerPod,Timeout,number=0)
		if algoAlt=="QoSMinCostImp":	
			#TODO: GitlabIssue #6
			print("FixSaturation is not yet implemented for algorithm alternative QoSMinCostImp")
		if algoAlt=="QoSFitness":	
			Best=voila.FixSaturationQoSFitness(Nodes,NodeObjects,SelectedNodes,start,
								TempCase,TestMat,
								ProbMat,LatMat,ListReq,
								TotalReq,L,VProximity,Ep0,Cs,Ca,fitnessAlpha,
								VImbalance,SaturationPerPod,Timeout,number=0)

		if Best.Ep<Ep0:
			#print("A replacement is Done")
			time.sleep(sleep)
		else:
			print("No Solution was found")

	# Potential over-provisioning
	if (Ep0>TempCase.Ep and len(TempCase.Nodes)> (TotalReq/SaturationPerPod)+3) and not Pr:
		Violation="Provisioning"
		#Studied=nc	
		print("Checking for a scale down possibility",len(TempCase.Nodes)-(TotalReq/SaturationPerPod))
		
		#Switch according to algorithm chosen for evaluation
		if algoAlt=="QoSOnly":	
			Best=voila.CheckscaleDown(TempCase,ProbMat,LatMat,TestMat,Nodes,ListReq,
						TotalReq,SelectedNodes,L,Ep0,LPC,
						VProximity=99.9, VImbalance=5,SaturationPerPod=SaturationPerPod,Timeout=1)
		if algoAlt=="QoSMinCost":	
			Best=voila.CheckscaleDownWithS(TempCase,TempCase,NodeObjects,ProbMat,LatMat,TestMat,Nodes,ListReq,
						TotalReq,SelectedNodes,L,Ep0,Cs,Ca,LPC,
						VProximity=99.9, VImbalance=5,SaturationPerPod=SaturationPerPod,Timeout=1)
		if algoAlt=="QoSMinCostImp":	
			#TODO: GitlabIssue #7
			Best=voila.CheckscaleDown(TempCase,ProbMat,LatMat,TestMat,Nodes,ListReq,
						TotalReq,SelectedNodes,L,Ep0,LPC,
						VProximity=99.9, VImbalance=5,SaturationPerPod=SaturationPerPod,Timeout=1)
		if algoAlt=="QoSFitness":	
			Best=voila.CheckscaleDownWithS(TempCase,TempCase,NodeObjects,ProbMat,LatMat,TestMat,Nodes,ListReq,
						TotalReq,SelectedNodes,L,Ep0,Cs,Ca,LPC,
						VProximity=99.9, VImbalance=5,SaturationPerPod=SaturationPerPod,Timeout=1)
		if algoAlt=="IgnoreS":	
			Best=voila.CheckscaleDown(TempCase,ProbMat,LatMat,TestMat,Nodes,ListReq,
						TotalReq,SelectedNodes,L,Ep0,LPC,
						VProximity=99.9, VImbalance=5,SaturationPerPod=SaturationPerPod,Timeout=1)

		if Best==TempCase: 
			print("Didn't scale down")
		else: 
			time.sleep(sleep)
	time.sleep(sleep)

	if Best.Nodes!=TempCase.Nodes: 
		if len(Best.Nodes)==len(TempCase.Nodes): 
			print("Execute a Replacement")
			Change="Replace"
		
		if len(Best.Nodes)>len(TempCase.Nodes): #scaled up 
			print("Execute Scale Up")
			NodesToBeAssigned=[]
			NodeToBeReplaced=""
			for node in Best.Nodes: 
				if node not in TempCase.Nodes: 
					NodesToBeAssigned.append(node)
			for node in TempCase.Nodes: 
				if node not in Best.Nodes:
					NodeToBeReplaced=node
					break
			if NodeToBeReplaced=="": 
				Change="Up"
			else:
				Change="Up/Replace"
		NodesToBeRemoved=[]
		if len(Best.Nodes)<len(TempCase.Nodes):
			Change="Down" 
			print("Execute Scale Down")
	Utilization=TotalReq*100/(len(SelectedNodes)*Actual)
	cost=voila.CalculateCost(Best,TempCase,NodeObjects,Cs,Ca)
	
	row=[cycle,len(TempCase.Nodes),algoAlt,TempCase.Ep,AEp,TempCase.Vl0,TempCase.Vc0,AVc0,TempCase.MaxLPP,Utilization,TotalReq,Violation,Change,Studied,Ep0,L,SaturationPerPod,Tol,cost,NumberS,Ps,Cs,Ca,fitnessAlpha]
	row2=[cycle]+[node for node in SelectedNodes]
	WriteCsvRow(f,row)
	WriteCsvRow(g,row2)

	Pr=True
	if Change=="None" or Change=="Down": 
		Pr =False

	return Best,TotalReq,TotalLostCycle,Pr
