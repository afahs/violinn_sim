#!/usr/bin/python3
import numpy as np
import hona
import time
import math
from operator import attrgetter

Cr=0.2
#Ca=0.5
Cd=1
#Cs=2

# Temp global variable for testing:
# this variable limits the numbet of recursion iteration
MAX_Iter= 3
TIMEOUT = 5 # in seconds


def GetNodeType(Name,NodeObjects): 
	for node in NodeObjects: 
		if node.name==Name: 
			return node.type
	assert False, "A node Name was not found"
	
def ReturnNumberS(SelectedNodes,NodeObjects): 
	count=0
	for Node in SelectedNodes: 
		if GetNodeType(Node,NodeObjects)=="S":
			count+=1
	return count 

def GetNodesOfType(nodeType,NodeObjects):
	nodesOfType=[]
	for node in NodeObjects:
		if node.type==nodeType:
			nodesOfType.append(node)
	return nodesOfType	
	
def GetRemovedNodes(Old, New): 
	Removed=[]
	for node in Old: 
		if node not in New: 
			Removed.append(node)
	return Removed

def ReplacementCost(AddedNode,RemovedNode,NodeObjects,Cs,Ca): 
	typeA=GetNodeType(AddedNode,NodeObjects)
	typeR=GetNodeType(RemovedNode,NodeObjects)
	if typeA=="D" and typeR=="D":
		Cost=Cr
	if typeA=="S" and typeR=="S":
		Cost=Cr+Ca
	if typeA=="D" and typeR=="S":
		Cost=-Cs+Cd+Cr
	if typeA=="S" and typeR=="D":
		Cost=Cr+Ca+Cs-Cd
	return Cost

def CalculateChangeCost(Added,Removed,NodeObjects,Cs,Ca): 
	if len(Added)==len(Removed) and len(Removed)!=0: #Replacement
		for i in range(len(Added)):
			Cost=0
			Cost+=ReplacementCost(Added[i],Removed[i],NodeObjects,Cs,Ca)
	if Added==[] or Removed==[]:
		if Removed==[] and Added==[]: # No Change 
			Cost=0
		if Removed!=[]:
			Cost=0 
			for Node in Removed: 
				if GetNodeType(Node,NodeObjects)=="S": 
					Cost+=-Cs
				if GetNodeType(Node,NodeObjects)=="D": 
					Cost+=-Cd
		if Added!=[]: # Scale without replacement
			Cost=0 
			for Node in Added:
				if GetNodeType(Node,NodeObjects)=="S": 
					Cost+=+Cs+Cr+Ca
				if GetNodeType(Node,NodeObjects)=="D": 
					Cost+=+Cd+Cr
	else: 
		if len(Added)>len(Removed): 
			Cost=0
			for i in range(len(Removed)): 
				Cost+=ReplacementCost(Added[i],Removed[i],NodeObjects,Cs,Ca)
			for i in range(len(Removed)):
				Added.remove(Added[0])
			for Node in Added: 
				if GetNodeType(Node,NodeObjects)=="S": 
					Cost+=+Cs+Cr+Ca
				if GetNodeType(Node,NodeObjects)=="D": 
					Cost+=+Cd+Cr
		else: 
			if len(Added)<len(Removed): 
				Cost=0
				for i in range(len(Added)): 
					Cost+=ReplacementCost(Added[i],Removed[i],NodeObjects,Cs,Ca)
				for i in range(len(Added)):
					Removed.remove(Removed[0])
				for Node in Removed: 
					if GetNodeType(Node,NodeObjects)=="S": 
						Cost+=-Cs
					if GetNodeType(Node,NodeObjects)=="D": 
						Cost+=-Cd	
	return Cost

def CalculateOldCost(Old,NodeObjects,Cs,Ca): 
	Cost=0
	for node in Old: 
		if GetNodeType(node,NodeObjects) == "S": 
			Cost+=Cs
		else: 
			Cost+=Cd
	return Cost

def CalculateCost(Best,TempCase,NodeObjects,Cs,Ca):
	Old=TempCase.Nodes
	Cost=CalculateOldCost(Old,NodeObjects,Cs,Ca)
	#print(Cost)
	New=Best.Nodes
	# print(Old)
	# print([GetNodeType(node,NodeObjects) for node in Old] )
	# print(New)
	# print([GetNodeType(node,NodeObjects) for node in New] )
	if Old != New: 
		Removed=GetRemovedNodes(Old, New)
		Added=GetRemovedNodes(New, Old)
		Cost+=CalculateChangeCost(Added,Removed,NodeObjects,Cs,Ca)	
	#print(Cost)
	#OBS: returning the accumulated cost
	return Cost
	
def CalculateFitness(Current,Previous,NodeObjects,Epsilon,Cs,Ca,fitnessAlpha):
	Cost=CalculateCost(Current,Previous,NodeObjects,Cs,Ca)
	#We want to also have the cost from 0 to 100
	nbDedicated=len(GetNodesOfType("D",NodeObjects))
	nbSpare=len(GetNodesOfType("S",NodeObjects))
	#minCost=Cd
	maxCost=(nbDedicated-1)*(Cd+Cr)+Cd+nbSpare*(Cs+Ca+Cr)
	#percentCost=((Cost-minCost)/(maxCost-minCost))*100
	#alpha=0.5 #Fitness parameter
	#fitness=fitnessAlpha*(100-Epsilon)+(1-fitnessAlpha)*percentCost
	percentCost=(Cost/maxCost)
	fitness=fitnessAlpha*Epsilon+(1-fitnessAlpha)*percentCost

	
	return fitness


def TestCondition(Case,start,VProximity,Ep0,SaturationPerPod,number,timeout=TIMEOUT,MAX_Iter=MAX_Iter): 
	if Case.Ep<Ep0:#and Case.Imbalance<VImbalance:
		print(Case.Nodes)
		print("A solution was found")
		#print(Case.MaxLPP,Case.Proximity,Case.Nodes) 
		return True,False
	if time.time()-start>timeout: # time in time
		print("Timeout") 
		return True,True
	if MAX_Iter<number: # time out in the number of recursions
		print("Max Iterations reached => abort") 
		return True,True
	return False,False



def GetScores(Nodes,NodeObjects,Indecies,ToBeAssigned,FullTestMat,Unique,UNodes,Replace,With):
	# ADD=[]
	# for i in range(len(FullTestMat)): 
	# 	if i not in Indecies: 
	# 		ADD.append(i)
	scores=[]
	ToBeTested={}
	#print(len(ADD))
	NotUnique=[i for i in Indecies if (i not in UNodes) and (GetNodeType(Nodes[i],NodeObjects)==Replace)] # those nodes can be replaced without affecting the Proximity

	# if NotUnique!=[]:
	# 	for node in NotUnique: 

	temp=FullTestMat[ToBeAssigned,:] #ToBeAssigned are the nodes that have slow latency
	y=np.sum(temp,axis=0) # for every available gateway node, get how many fast link the gateway can provide for the TBA
	for i in range(len(y)):
		if (i not in Indecies) and (y[i]!=0 ) and (GetNodeType(Nodes[i],NodeObjects)==With): # Selected nodes are excluded, and gateways with 0 fast links
			scores.append((i,y[i])) # A score for every possible gateway
	scores.sort(key=lambda tup: tup[1], reverse=True) 
	Newscores=[]
	for i in range(len(scores)): 
		if FullTestMat[ToBeAssigned[0],scores[i][0]] and i<11: # select only the first 10 nodes that can serve the first TBA 
			Newscores.append(scores[i][0])
	for i in NotUnique: # Try to replace an unimportant node by one of the nodes with high score
		ToBeTested[i]=Newscores
	return ToBeTested # returns the potential replacement

def TestReplaceProximity(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Slow,Servers,Indecies,Continue,Replace="S",With="D"):
	print("ENTERED THE TEST PROXIMITY REPLACEMENT WITH REPLACE:", Replace, "WITH", With)
	start=time.time()
	# print(SelectedNodes)
	# print([GetNodeType(node,NodeObjects) for node in SelectedNodes])
	# print([Nodes.index(node) for node in SelectedNodes])
	Cases=[]
	ToBeAssigned=[]
	i=0
	for k,v in Slow.items():
		if i> len(Slow)/5: 
			break
		ToBeAssigned.append(k)
		i+=1
	Unique,UNodes=GetUnique(TestMat,Indecies,Servers,Nodes)
	ToBeTested=GetScores(Nodes,NodeObjects,Indecies,ToBeAssigned,FullTestMat,Unique,UNodes,Replace,With)
	Cases=[]
	# for k,v in ToBeTested.items(): 
	# 	print(k,v)
	for key,li in ToBeTested.items():
		Indecies.remove(key)
		for i in li:
			#print(NewIndecies)
			Indecies.append(i)
			Indecies.sort()
			#print(NewIndecies)
			SelectedNodes=[Nodes[i] for i in Indecies ]
			Temp=hona.CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,SaturationPerPod,Number=0)
			Temp.Cost=CalculateCost(Temp,TempCase,NodeObjects,Cs,Ca)
			#print("Ep:",TempCase.Ep,"Vl0:",TempCase.Vl0,"Vc0",TempCase.Vc0,"MaxLPP:",TempCase.MaxLPP,"Total:",sum(TempCase.RPP),[Nodes.index(node) for node in SelectedNodes])
			Cases.append(Temp)
			if not Continue:
				if hona.TestCondition(Temp,start,0,Ep0,SaturationPerPod,2)[0]:
					print("Not Continue: A solution is found, stop testing the cases.")
					return Cases

			Indecies.remove(i)
		Indecies.append(key)
		Indecies.sort()
	return Cases

def TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Continue,Replace="S",With="D"):
	print("ENTERED THE TEST SATURATION REPLACEMENT WITH REPLACE:", Replace, "WITH", With)
	Cases=[]
	RPPNode=[]
	ToBeTested={}
	Servers={}
	Indecies=[] # selected nodes Indecies
	start=time.time() 
	# print(SelectedNodes)
	# print([GetNodeType(node,NodeObjects) for node in SelectedNodes])
	# print([Nodes.index(node) for node in SelectedNodes])
	for node in SelectedNodes:
		Index=Nodes.index(node)
		Indecies.append(Index)
		Servers[Index]=sum(TestMat[:,Index])
		
	for i in range(len(TempCase.Nodes)):
		if GetNodeType(TempCase.Nodes[i] ,NodeObjects) ==  Replace:
			RPPNode.append((Nodes.index(TempCase.Nodes[i]),TempCase.RPP[i]))
	if RPPNode== []: 
		return []
	RPPNode.sort(key=lambda tup: tup[1]) # load per pod indexed by the server node index and sorted in an increasing order
	Unique,UNodes=GetUnique(TestMat,Indecies,Servers,Nodes)
	NexTo=RPPNode[len(RPPNode)-1][0] # node with the highest load (Origin)
	for i in range(len(RPPNode)-1):
		ToBeReplaced=RPPNode[i][0]
		ProperNodes=GetProperNodes(ToBeReplaced,Indecies,Nodes,FullTestMat,NexTo,Unique,UNodes)
		NewProper=[]
		for node in ProperNodes:
			if GetNodeType(Nodes[node],NodeObjects) == With: 
				NewProper.append(node)
		ToBeTested[ToBeReplaced]=NewProper
	Cases=[]
	for key,li in ToBeTested.items():

		if li!=[]:

			Indecies.remove(key)

			for i in li:
				Indecies.append(i)
				Indecies.sort()
				SelectedNodes=[Nodes[i] for i in Indecies ]
				Temp=hona.CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,SaturationPerPod,Number=0)
				Temp.Cost=CalculateCost(Temp,TempCase,NodeObjects,Cs,Ca)
				Cases.append(Temp)
				if not Continue:
					if hona.TestCondition(Temp,start,0,Ep0,SaturationPerPod,2)[0]:
						print("Not Continue: A solution is found, stop testing the cases.")
						return Cases

				Indecies.remove(i)

			Indecies.append(key)
			Indecies.sort()
	return Cases



def GetUnique(TestMat,Indecies,Servers,Nodes): # optimize
	temp=TestMat[:,Indecies]
	Unique=[]
	for i in range(len(Indecies)):
		li=[] 
		for j in range(len(temp)): 
			if temp[j,i]==True: 
				if sum(temp[j])==1: 
					li.append(j)
		if len(li)!=0: 
			Unique.append((Indecies[i],li,len(li)))# the unique nodes and the gateways that depends on them
	NewIndecies=[]
	NewIndecies=[item[0] for item in Unique] # the unique nodes 
	#NewIndecies=Indecies
	Unique.sort(key=lambda tup: tup[2])
	return Unique,NewIndecies 

def GetProperNodes(ToBeReplaced,Indecies,Nodes,FullTestMat,NexTo,Unique,UNodes):
	# IN(Isolated Nodes) is the nodes who have TBR as the only server
	ProperNodes=[]
	if ToBeReplaced in UNodes: 
		IN= [item for item in Unique if item[0]==ToBeReplaced][0][1]# IN(Isolated Nodes) is the nodes who have TBR as the only server
		temp=FullTestMat[IN,:]
		for i in range(len(temp[0])): 
			if i==ToBeReplaced: 
				continue
			Flag=True
			for j in range(len(temp)): 
				if not temp[j,i]:
					Flag=False
					break
			if Flag: 
				ProperNodes.append(i)
	else:
		#print(ToBeReplaced)
		ProperNodes=list(range(len(Nodes)))
		ProperNodes.remove(ToBeReplaced)
	NewProper=[]
	for node in ProperNodes: 
		if FullTestMat[NexTo,node] and node not in Indecies: 
			NewProper.append(node)
	return NewProper

def GetBest2(Cases,SaturationPerPod,VProximity):
	"""
	Compute the objective function result OMEGA
	and then returns the case with the Max value of Omega (Best Case)
		Cases 	=> A list of objects Case
		a 		=> The value Alpha defined by Hona to control the trade-off 
				   between Proximity and Imbalance
	"""
	MinEp=100
	BestCase=Cases[0]
	for case in Cases:
		case.Vl0=sum(case.RPP)*(100-case.Proximity)/100
		case.Vc0=0
		for p in case.RPP: 
			if p > SaturationPerPod: 
				case.Vc0+=p-SaturationPerPod
		case.Ep=(case.Vc0+case.Vl0)*100/sum(case.RPP)
		#print("Vl0",case.Vc0,"Vc0",case.Vc0,"Ep",case.Ep,"total",sum(case.RPP),case.Nodes)
		if case.Ep<MinEp: 
			MinEp=case.Ep
			BestCase=case
	return BestCase
	
def GetBestWithCost(Cases,SaturationPerPod,VProximity,Ep0, Previous, NodeObjects):
	MinEp=100
	BestCase=Cases[0]
	SuitableCases=[]
	for case in Cases:
		case.Vl0=sum(case.RPP)*(100-case.Proximity)/100
		case.Vc0=0
		for p in case.RPP: 
			if p > SaturationPerPod: 
				case.Vc0+=p-SaturationPerPod
		case.Ep=(case.Vc0+case.Vl0)*100/sum(case.RPP)
		#print("Vl0",case.Vc0,"Vc0",case.Vc0,"Ep",case.Ep,"total",sum(case.RPP),case.Nodes)
		if case.Ep<MinEp: 
			MinEp=case.Ep
			BestCase=case
		#Also get all the suitable cases (i.e. below the threshold)
		if case.Ep < Ep0:
			SuitableCases.append(case)
	
	if SuitableCases==[]: 
		for case in Cases:
			if case.Ep < MinEp+0.1: 
				SuitableCases.append(case)

	#If we have suitable cases, we want to return the one with minimum cost as the best
	#Otherwise, we return the current best
	if SuitableCases != []:
		MinCostCase=min_num = min(SuitableCases,key=attrgetter('Ep'))
		MinCost=MinCostCase.Cost
		# MinCost=SuitableCases[0].Cost
		# MinCostCase=SuitableCases[0]
		for case in SuitableCases:
			print(case.Ep, case.Cost, len(case.Nodes),[GetNodeType(node,NodeObjects) for node in case.Nodes])
			if case.Cost<MinCost: 
				MinCost=case.Cost
				MinCostCase=case
		BestCase=MinCostCase

		
	return BestCase
	
def GetBestWithFitness(Cases,SaturationPerPod,VProximity,Ep0, Previous, NodeObjects,Cs,Ca,fitnessAlpha):
	assert fitnessAlpha>=0 and fitnessAlpha<=1; "The value of alpha "+ str(fitnessAlpha)+" is out of range" 
	MinEp=100
	BestCase=Cases[0]
	SuitableCases=[]
	for case in Cases:
		case.Vl0=sum(case.RPP)*(100-case.Proximity)/100
		case.Vc0=0
		for p in case.RPP: 
			if p > SaturationPerPod: 
				case.Vc0+=p-SaturationPerPod
		case.Ep=(case.Vc0+case.Vl0)*100/sum(case.RPP)
		#print("Vl0",case.Vc0,"Vc0",case.Vc0,"Ep",case.Ep,"total",sum(case.RPP),case.Nodes)
		if case.Ep<MinEp: 
			MinEp=case.Ep
			BestCase=case
		#Also get all the suitable cases (i.e. below the threshold)
		if case.Ep < Ep0:
			SuitableCases.append(case)
	#If we have suitable cases, we want to return the one with minimum cost as the best
	#Otherwise, we return the current best
	if SuitableCases==[]: 
		for case in Cases:
			if case.Ep < MinEp+0.1: 
				SuitableCases.append(case)
	if SuitableCases != []:
		MaxFitness=CalculateFitness(SuitableCases[0],Previous,NodeObjects,SuitableCases[0].Ep,Cs,Ca,fitnessAlpha)
		MaxFitnessCase=SuitableCases[0]
		for case in SuitableCases:
			case.Fitness=CalculateFitness(case,Previous,NodeObjects,case.Ep,Cs,Ca,fitnessAlpha)
			if case.Fitness<MaxFitness: 
				MaxFitness=case.Fitness
				MaxFitnessCase=case
		BestCase=MaxFitnessCase
		
	return BestCase

def CalculateSlowPerGateway(LatMat,Nodes,SelectedNodes,Temp,L,ListReq): 
	Slow={}
	for i in range(len(Nodes)):
		Slow[i]=0
	for i in range(len(Nodes)):
		for j in range(len(SelectedNodes)):
			Req=Temp[i,j]*ListReq[i]
			IND=Nodes.index(SelectedNodes[j])
			if LatMat[i,IND] > L:
				Slow[i]+=Req
	Slow={k:v for k,v in Slow.items() if v!=0}
	Slow={k:v for k,v in sorted(Slow.items(), key=lambda item: item[1], reverse=True)}

	return Slow

def AutoScale(VProximity,VImbalance,SaturationPerPod,Timeout,TempCase,ProbMat,TestMat,NodeObjects,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,Cs,Ca,NewIndecies,Number,start,Continue,Reason="Sat",Add="D"): 
	Upscale=1
	NewCases=[]
	#print(NewIndecies)
	for node in Nodes: 
		if Nodes.index(node) not in NewIndecies and GetNodeType(node,NodeObjects) == Add: 
			NewIndecies.append(Nodes.index(node))
			NewIndecies.sort()
			SelectedNodes=[Nodes[i] for i in NewIndecies ]
			Temp=hona.CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,SaturationPerPod)
			Temp.Cost=CalculateCost(Temp,TempCase,NodeObjects,Cs,Ca)
			Number+=1
			NewCases.append(Temp)
			#print(TempCase.Proximity,TempCase.Imbalance,TempCase.MaxLPP,[Nodes.index(node) for node in SelectedNodes])
			if not Continue:
				if hona.TestCondition(Temp,start,VProximity,VImbalance,SaturationPerPod,Timeout)[0]:
					print("Not Continue: A solution is found, stop testing the cases.")
					return NewCases,Number,True
			#print(Proximity,Imbalance,"Scale up "+ str(Upscale))
			NewIndecies.remove(Nodes.index(node))
	return NewCases,Number,False

############################################################### IgnoreS ######################################################################################################

def FixSaturationIgnoreS(Nodes,NodeObjects,SelectedNodes,start,TempCase,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,VImbalance,SaturationPerPod,Timeout,number=0):
	start=time.time()
	print("FixSaturationIgnoreS Recursion " + str(number) + " len " + str(len(TempCase.Nodes)))
	Cases=[]
	Spare=[node.name for node in NodeObjects if node.type=="S"]
	Dedicated=[node for node in Nodes if node not in Spare]
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	IdleGateways=[]
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)
	start=time.time()
	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)

	Continue=False
	Cases+=TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Continue,Replace="D",With="D")

	if len(Cases)!=0: 
		Best=GetBest2(Cases,SaturationPerPod,VProximity)
	else: 
		Best=TempCase

	if Best.Ep<Ep0: # if a solution was found with replacement no need to test the scaling up (now this need to be improved in COST in later function)
		if number!=0: # number=0 should return the solution, in this case else will be skipped and the function will go to the if number==0 directly
			print("debug 1")
			return Cases
		else: 
			return Best 			
	else: # NO replacement solution was found, we have to scale up
		Indecies=[Nodes.index(n) for n in Best.Nodes]
		NewCases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,TempCase,ProbMat,TestMat,NodeObjects,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Cs,Ca,
									Indecies,0,start,Continue,
									Reason="Sat",Add="D") 
		Cases+=NewCases

	if len(Cases)!=0: 
		Best=GetBest2(Cases,SaturationPerPod,VProximity)
	else: 
		Best=TempCase
	#print(Best.Ep,"Len of Best",len(Best.Nodes),"Len of Temp",len(TempCase.Nodes))
	if not TestCondition(Best,start,0,Ep0,SaturationPerPod,number)[0]: # is the any solutions in the tested Cases
		# if No solution is found in the cases, the we need to do a new loop iteration
		#Timeout=Timeout-(time.time()-start)
		Cases+=FixSaturationIgnoreS(Nodes,NodeObjects,Best.Nodes,start,Best,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,VImbalance,SaturationPerPod,Timeout,number=number+1)
		Best=GetBest2(Cases,SaturationPerPod,VProximity)

	if number==0: # in case of the first Recursion return the solution and not the cases
		# for case in Cases: 
	 # 		print(case.Ep,case.Cost,case.Nodes)
		Best=GetBest2(Cases,SaturationPerPod,VProximity) 
		return Best
	print("debug 2")
	return Cases


def FixProximityIgnoreS(Nodes,NodeObjects,SelectedNodes,start,TempCase,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,VImbalance,SaturationPerPod,Timeout,number=0): 
	print("FixProximityIgnoreS Recursion " + str(number))
	Cases=[]
	Spare=[node.name for node in NodeObjects if node.type=="S"]
	Dedicated=[node for node in Nodes if node not in Spare]
	Servers={}
	Indecies=[]
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	IdleGateways=[]
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)

	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
		Indecies.append(Index)
	Slow=CalculateSlowPerGateway(LatMat,Nodes,SelectedNodes,TempCase.ProbMat,L,ListReq)
	
	Continue=False
	Cases+=TestReplaceProximity(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Slow,Servers,Indecies,Continue,Replace="D",With="D")	
	
	if len(Cases)!=0: 
		Best=GetBest2(Cases,SaturationPerPod,VProximity)
	else: 
		Best=TempCase

	if Best.Ep<Ep0: # if a solution was found with replacement no need to test the scaling up (now this need to be improved in COST in later function)
		if number!=0:  # number=0 should return the solution, in this case else will be skipped and the function will go to the if number==0 directly
			print("debug 3")
			return Cases
		else: 
			return Best

	else:# NO replacement solution was found, we have to scale up

		Indecies=[Nodes.index(n) for n in Best.Nodes]
		NewCases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,TempCase,ProbMat,TestMat,NodeObjects,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Cs,Ca,
									Indecies,0,start,Continue,
									Reason="Sat",Add="D") 
		Cases+=NewCases

	if len(Cases)!=0: 
		Best=GetBest2(Cases,SaturationPerPod,VProximity)
	else: 
		Best=TempCase

	if not TestCondition(Best,start,0,Ep0,SaturationPerPod,number)[0]: # is the any solutions in the tested Cases
		# if No solution is found in the cases, the we need to do a new loop iteration
		Cases+=FixProximityIgnoreS(Nodes,NodeObjects,Best.Nodes,start,Best,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,VImbalance,SaturationPerPod,Timeout,number=number+1)
		Best=GetBest2(Cases,SaturationPerPod,VProximity)

	if number==0: # in case of the first Recursion return the solution and not the cases
		# for case in Cases: 
		# 	print(case.Ep,case.Cost,case.Nodes)
		Best=GetBest2(Cases,SaturationPerPod,VProximity) 
		return Best
	print("debug 4")
	return Cases

############################################################### QOS ######################################################################################################

def FixSaturationQoS(Nodes,NodeObjects,SelectedNodes,start,TempCase,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,VImbalance,SaturationPerPod,Timeout,number=0):
	print("FixSaturationQoS Recursion " + str(number))
	print("Len of selected: "+ str(len(SelectedNodes)))
	Cases=[]
	Spare=[node.name for node in NodeObjects if node.type=="S"]
	Dedicated=[node for node in Nodes if node not in Spare]
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	IdleGateways=[]
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)
	start=time.time()
	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	Continue=False
	if list(set.intersection(set(Spare), set(SelectedNodes))) != []: 
		Cases+=TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Continue,Replace="S",With="D")

	Cases+=TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Continue,Replace="D",With="D")

	if list(set.intersection(set(Spare), set(SelectedNodes))) != []: 
		Cases+=TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Continue,Replace="S",With="S")

	Cases+=TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Continue,Replace="D",With="S")

	if len(Cases)!=0: 
		Best=GetBest2(Cases,SaturationPerPod,VProximity)
	else: 
		Best=TempCase

	if Best.Ep<Ep0: # if a solution was found with replacement no need to test the scaling up (now this need to be improved in COST in later function)
		if number!=0: # number=0 should return the solution, in this case else will be skipped and the function will go to the if number==0 directly
			print("debug 5")
			return Cases
		else: 
			return Best
	else: # NO replacement solution was found, we have to scale up
		Indecies=[Nodes.index(n) for n in Best.Nodes]
		NewCases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,TempCase,ProbMat,TestMat,NodeObjects,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Cs,Ca,
									Indecies,0,start,Continue,
									Reason="Sat",Add="D") 
		Cases+=NewCases
		# Best=GetBest2(Cases,SaturationPerPod,VProximity)
		# if Best.Ep<Ep0: # if a solution was found without adding S node then don't test S nodes (more Expensive) 
		# 	if number!=0:  # number=0 should return the solution, in this case else will be skipped and the function will go to the if number==0 directly
		# 		return Cases
		# else: # No scale up with one D solution was found, we have to try S
		NewCases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,TempCase,ProbMat,TestMat,NodeObjects,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Cs,Ca,
									Indecies,0,start,Continue,
									Reason="Sat",Add="S") 
		Cases+=NewCases
	if len(Cases)!=0: 
		Best=GetBest2(Cases,SaturationPerPod,VProximity)
	else: 
		Best=TempCase
	#print(Best.Ep,"Len of Best",len(Best.Nodes),"Len of Temp",len(TempCase.Nodes))
	if not TestCondition(Best,start,0,Ep0,SaturationPerPod,number)[0]: # is the any solutions in the tested Cases
		# if No solution is found in the cases, the we need to do a new loop iteration
		Cases+=FixSaturationQoS(Nodes,NodeObjects,Best.Nodes,start,Best,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,VImbalance,SaturationPerPod,Timeout,number=number+1)
		Best=GetBest2(Cases,SaturationPerPod,VProximity)

	if number==0: # in case of the first Recursion return the solution and not the cases
		# for case in Cases: 
		# 	print(case.Ep,case.Cost,case.Nodes)
		Best=GetBest2(Cases,SaturationPerPod,VProximity) 
		return Best
	print("debug 6")
	return Cases


def FixProximityQoS(Nodes,NodeObjects,SelectedNodes,start,TempCase,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,VImbalance,SaturationPerPod,Timeout,number=0): 
	print("FixProximityQoS Recursion " + str(number))
	Cases=[]
	Spare=[node.name for node in NodeObjects if node.type=="S"]
	Dedicated=[node for node in Nodes if node not in Spare]
	Servers={}
	Indecies=[]
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	IdleGateways=[]
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)

	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
		Indecies.append(Index)
	Slow=CalculateSlowPerGateway(LatMat,Nodes,SelectedNodes,TempCase.ProbMat,L,ListReq)
	Continue=False
	if list(set.intersection(set(Spare), set(SelectedNodes))) != []: 
			Cases+=TestReplaceProximity(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Slow,Servers,Indecies,Continue,Replace="S",With="D")
	Cases+=TestReplaceProximity(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Slow,Servers,Indecies,Continue,Replace="D",With="D")	
	
	if list(set.intersection(set(Spare), set(SelectedNodes))) != []: 
		Cases+=TestReplaceProximity(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Slow,Servers,Indecies,Continue,Replace="S",With="S")	
	Cases+=TestReplaceProximity(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Slow,Servers,Indecies,Continue,Replace="D",With="S")	
	
	if len(Cases)!=0: 
		Best=GetBest2(Cases,SaturationPerPod,VProximity)
	else: 
		Best=TempCase

	if Best.Ep<Ep0: # if a solution was found with replacement no need to test the scaling up (now this need to be improved in COST in later function)
		if number!=0:  # number=0 should return the solution, in this case else will be skipped and the function will go to the if number==0 directly
			print("debug 7")
			return Cases
		else: 
			return Best

	else:# NO replacement solution was found, we have to scale up

		Indecies=[Nodes.index(n) for n in Best.Nodes]
		NewCases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,TempCase,ProbMat,TestMat,NodeObjects,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Cs,Ca,
									Indecies,0,start,Continue,
									Reason="Sat",Add="D") 
		Cases+=NewCases
		# Best=GetBest2(Cases,SaturationPerPod,VProximity)
		# if Best.Ep<Ep0: # if a solution was found without adding S node then don't test S nodes (more Expensive) 
		# 	if number!=0:  # number=0 should return the solution, in this case else will be skipped and the function will go to the if number==0 directly
		# 		return Cases

		# else: 
		NewCases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,TempCase,ProbMat,TestMat,NodeObjects,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Cs,Ca,
									Indecies,0,start,Continue,
									Reason="Sat",Add="S") 
		Cases+=NewCases

	if len(Cases)!=0: 
		Best=GetBest2(Cases,SaturationPerPod,VProximity)
	else: 
		Best=TempCase

	if not TestCondition(Best,start,0,Ep0,SaturationPerPod,number)[0]: # is the any solutions in the tested Cases
		# if No solution is found in the cases, the we need to do a new loop iteration
		Cases+=FixProximityQoS(Nodes,NodeObjects,Best.Nodes,start,Best,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,VImbalance,SaturationPerPod,Timeout,number=number+1)
		Best=GetBest2(Cases,SaturationPerPod,VProximity)


	if number==0: # in case of the first Recursion return the solution and not the cases
		# for case in Cases: 
		# 	print(case.Ep,case.Cost,case.Nodes)
		Best=GetBest2(Cases,SaturationPerPod,VProximity) 
		return Best
	print("debug 8")
	return Cases

############################################################### QoSMinCost ######################################################################################################

def FixSaturationQoSMinCost(Nodes,NodeObjects,SelectedNodes,start,TempCase,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,VImbalance,SaturationPerPod,Timeout,number=0):
	print("FixSaturationQoSMinCost Recursion " + str(number))
	print([GetNodeType(node,NodeObjects) for node in SelectedNodes])
	Cases=[]
	Spare=[node.name for node in NodeObjects if node.type=="S"]
	Dedicated=[node for node in Nodes if node not in Spare]
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	IdleGateways=[]
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)
	start=time.time()
	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	Continue=True
	if list(set.intersection(set(Spare), set(SelectedNodes))) != []: 
		Cases+=TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Continue,Replace="S",With="D")

	Cases+=TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Continue,Replace="D",With="D")

	if list(set.intersection(set(Spare), set(SelectedNodes))) != []: 
		Cases+=TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Continue,Replace="S",With="S")

	Cases+=TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Continue,Replace="D",With="S")

	if len(Cases)!=0: 
		Best=GetBestWithCost(Cases,SaturationPerPod,VProximity,Ep0,TempCase,NodeObjects)
	else: 
		Best=TempCase

	if Best.Ep<Ep0: # if a solution was found with replacement no need to test the scaling up (now this need to be improved in COST in later function)
		if number!=0: # number=0 should return the solution, in this case else will be skipped and the function will go to the if number==0 directly
			print("debug 9")
			return Cases
		else: 
			return Best

	else: # NO replacement solution was found, we have to scale up
		Indecies=[Nodes.index(n) for n in Best.Nodes]
		NewCases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,TempCase,ProbMat,TestMat,NodeObjects,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Cs,Ca,
									Indecies,0,start,Continue,
									Reason="Sat",Add="D") 
		Cases+=NewCases
		Best=GetBestWithCost(Cases,SaturationPerPod,VProximity,Ep0,TempCase,NodeObjects)
		if Best.Ep<Ep0: # if a solution was found without adding S node then don't test S nodes (more Expensive) 
			if number!=0:  # number=0 should return the solution, in this case else will be skipped and the function will go to the if number==0 directly
				print("debug 10")
				return Cases
		else: # No scale up with one D solution was found, we have to try S
			NewCases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,TempCase,ProbMat,TestMat,NodeObjects,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Cs,Ca,
									Indecies,0,start,Continue,
									Reason="Sat",Add="S") 
			Cases+=NewCases
	if len(Cases)!=0: 
		Best=GetBestWithCost(Cases,SaturationPerPod,VProximity,Ep0,TempCase,NodeObjects)
	else: 
		Best=TempCase
	#print(Best.Ep,"Len of Best",len(Best.Nodes),"Len of Temp",len(TempCase.Nodes))
	if not TestCondition(Best,start,0,Ep0,SaturationPerPod,number)[0]: # is the any solutions in the tested Cases
		# if No solution is found in the cases, the we need to do a new loop iteration
		Cases+=FixSaturationQoSMinCost(Nodes,NodeObjects,Best.Nodes,start,Best,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,VImbalance,SaturationPerPod,Timeout,number=number+1)
		Best=GetBestWithCost(Cases,SaturationPerPod,VProximity,Ep0,TempCase,NodeObjects)

	if number==0: # in case of the first Recursion return the solution and not the cases
		# for case in Cases: 
	 # 		print(case.Ep,case.Cost,case.Nodes)
		Best=GetBestWithCost(Cases,SaturationPerPod,VProximity,Ep0,TempCase,NodeObjects)
		return Best
	print("debug 11")
	return Cases


def FixProximityQoSMinCost(Nodes,NodeObjects,SelectedNodes,start,TempCase,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,VImbalance,SaturationPerPod,Timeout,number=0): 
	print("FixProximityQoSMinCost Recursion " + str(number))
	Cases=[]
	Spare=[node.name for node in NodeObjects if node.type=="S"]
	Dedicated=[node for node in Nodes if node not in Spare]
	Servers={}
	Indecies=[]
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	IdleGateways=[]
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)

	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
		Indecies.append(Index)
	Slow=CalculateSlowPerGateway(LatMat,Nodes,SelectedNodes,TempCase.ProbMat,L,ListReq)
	Continue=True
	if list(set.intersection(set(Spare), set(SelectedNodes))) != []: 
			Cases+=TestReplaceProximity(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Slow,Servers,Indecies,Continue,Replace="S",With="D")
	Cases+=TestReplaceProximity(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Slow,Servers,Indecies,Continue,Replace="D",With="D")	
	
	if list(set.intersection(set(Spare), set(SelectedNodes))) != []: 
		Cases+=TestReplaceProximity(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Slow,Servers,Indecies,Continue,Replace="S",With="S")	
	Cases+=TestReplaceProximity(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Slow,Servers,Indecies,Continue,Replace="D",With="S")	
	
	if len(Cases)!=0: 
		Best=GetBestWithCost(Cases,SaturationPerPod,VProximity,Ep0,TempCase,NodeObjects)
	else: 
		Best=TempCase

	if Best.Ep<Ep0: # if a solution was found with replacement no need to test the scaling up (now this need to be improved in COST in later function)
		if number!=0:  # number=0 should return the solution, in this case else will be skipped and the function will go to the if number==0 directly
			print("debug 12")
			return Cases
		else: 
			return Best
	else:# NO replacement solution was found, we have to scale up

		Indecies=[Nodes.index(n) for n in Best.Nodes]
		NewCases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,TempCase,ProbMat,TestMat,NodeObjects,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Cs,Ca,
									Indecies,0,start,Continue,
									Reason="Sat",Add="D") 
		Cases+=NewCases
		Best=GetBestWithCost(Cases,SaturationPerPod,VProximity,Ep0,TempCase,NodeObjects)
		if Best.Ep<Ep0: # if a solution was found without adding S node then don't test S nodes (more Expensive) 
			if number!=0:  # number=0 should return the solution, in this case else will be skipped and the function will go to the if number==0 directly
				print("debug 13")
				return Cases

		else: 
			NewCases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,TempCase,ProbMat,TestMat,NodeObjects,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Cs,Ca,
									Indecies,0,start,Continue,
									Reason="Sat",Add="S") 
			Cases+=NewCases

	if len(Cases)!=0: 
		Best=GetBestWithCost(Cases,SaturationPerPod,VProximity,Ep0,TempCase,NodeObjects)
	else: 
		Best=TempCase

	if not TestCondition(Best,start,0,Ep0,SaturationPerPod,number)[0]: # is the any solutions in the tested Cases
		# if No solution is found in the cases, the we need to do a new loop iteration
		Cases+=FixProximityQoSMinCost(Nodes,NodeObjects,Best.Nodes,start,Best,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,VImbalance,SaturationPerPod,Timeout,number=number+1)
		Best=GetBestWithCost(Cases,SaturationPerPod,VProximity,Ep0,TempCase,NodeObjects)

	if number==0: # in case of the first Recursion return the solution and not the cases
		# for case in Cases: 
	 # 		print(case.Ep,case.Cost,case.Nodes)
		Best=GetBestWithCost(Cases,SaturationPerPod,VProximity,Ep0,TempCase,NodeObjects)
		return Best
	print("debug 14")
	return Cases

	
############################################################### QOSFitness ######################################################################################################

def FixSaturationQoSFitness(Nodes,NodeObjects,SelectedNodes,start,TempCase,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,fitnessAlpha,VImbalance,SaturationPerPod,Timeout,number=0):
	print("FixSaturationQoSFitness Recursion " + str(number))
	Cases=[]
	Spare=[node.name for node in NodeObjects if node.type=="S"]
	Dedicated=[node for node in Nodes if node not in Spare]
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	IdleGateways=[]
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)
	start=time.time()
	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	Continue=True
	if list(set.intersection(set(Spare), set(SelectedNodes))) != []: 
		Cases+=TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Continue,Replace="S",With="D")

	Cases+=TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Continue,Replace="D",With="D")

	if list(set.intersection(set(Spare), set(SelectedNodes))) != []: 
		Cases+=TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Continue,Replace="S",With="S")

	Cases+=TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Continue,Replace="D",With="S")

	if len(Cases)!=0: 
		Best=GetBestWithFitness(Cases,SaturationPerPod,VProximity, Ep0, TempCase, NodeObjects, Cs,Ca,fitnessAlpha)
	else: 
		Best=TempCase

	if Best.Ep<Ep0: # if a solution was found with replacement no need to test the scaling up (now this need to be improved in COST in later function)
		if number!=0: # number=0 should return the solution, in this case else will be skipped and the function will go to the if number==0 directly
			print("debug 15")
			return Cases
		else: 
			return Best

	else: # NO replacement solution was found, we have to scale up
		Indecies=[Nodes.index(n) for n in Best.Nodes]
		NewCases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,TempCase,ProbMat,TestMat,NodeObjects,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Cs,Ca,
									Indecies,0,start,Continue,
									Reason="Sat",Add="D") 
		Cases+=NewCases
		if Cases!=[]: 
			Best=GetBestWithFitness(Cases,SaturationPerPod,VProximity, Ep0, TempCase, NodeObjects,Cs,Ca,fitnessAlpha)
		else: 
			Best=TempCase
			
		if Best.Ep<Ep0: # if a solution was found without adding S node then don't test S nodes (more Expensive) 
			if number!=0:  # number=0 should return the solution, in this case else will be skipped and the function will go to the if number==0 directly
				print("debug 16")
				return Cases
			else: 
				return Best
		else: # No scale up with one D solution was found, we have to try S
			NewCases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,TempCase,ProbMat,TestMat,NodeObjects,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Cs,Ca,
									Indecies,0,start,Continue,
									Reason="Sat",Add="S") 
			Cases+=NewCases
	if len(Cases)!=0: 
		Best=GetBestWithFitness(Cases,SaturationPerPod,VProximity, Ep0, TempCase, NodeObjects,Cs,Ca,fitnessAlpha)
	else: 
		Best=TempCase
	#print(Best.Ep,"Len of Best",len(Best.Nodes),"Len of Temp",len(TempCase.Nodes))
	if not TestCondition(Best,start,0,Ep0,SaturationPerPod,number)[0]: # is the any solutions in the tested Cases
		# if No solution is found in the cases, the we need to do a new loop iteration
		Cases+=FixSaturationQoSFitness(Nodes,NodeObjects,Best.Nodes,start,Best,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,fitnessAlpha,VImbalance,SaturationPerPod,Timeout,number=number+1)
		Best=GetBestWithFitness(Cases,SaturationPerPod,VProximity, Ep0, TempCase, NodeObjects,Cs,Ca,fitnessAlpha)

	if number==0: # in case of the first Recursion return the solution and not the cases
		# for case in Cases:
			#print(case.Ep,case.Cost,case.Nodes)
		Best=GetBestWithFitness(Cases,SaturationPerPod,VProximity, Ep0, TempCase, NodeObjects,Cs,Ca,fitnessAlpha) 
		return Best
	print("debug 17")
	return Cases


def FixProximityQoSFitness(Nodes,NodeObjects,SelectedNodes,start,TempCase,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,fitnessAlpha,VImbalance,SaturationPerPod,Timeout,number=0): 
	print("FixProximityQoSFitness Recursion " + str(number))
	Cases=[]
	Spare=[node.name for node in NodeObjects if node.type=="S"]
	Dedicated=[node for node in Nodes if node not in Spare]
	Servers={}
	Indecies=[]
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	IdleGateways=[]
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)

	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
		Indecies.append(Index)
	Slow=CalculateSlowPerGateway(LatMat,Nodes,SelectedNodes,TempCase.ProbMat,L,ListReq)
	Continue=True
	if list(set.intersection(set(Spare), set(SelectedNodes))) != []: 
			Cases+=TestReplaceProximity(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Slow,Servers,Indecies,Continue,Replace="S",With="D")
	Cases+=TestReplaceProximity(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Slow,Servers,Indecies,Continue,Replace="D",With="D")	
	
	if list(set.intersection(set(Spare), set(SelectedNodes))) != []: 
		Cases+=TestReplaceProximity(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Slow,Servers,Indecies,Continue,Replace="S",With="S")	
	Cases+=TestReplaceProximity(Nodes,NodeObjects,FullTestMat,SelectedNodes,TempCase,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,Slow,Servers,Indecies,Continue,Replace="D",With="S")	
	
	if len(Cases)!=0: 
		Best=GetBestWithFitness(Cases,SaturationPerPod,VProximity, Ep0, TempCase, NodeObjects,Cs,Ca,fitnessAlpha)
	else: 
		Best=TempCase

	if Best.Ep<Ep0: # if a solution was found with replacement no need to test the scaling up (now this need to be improved in COST in later function)
		if number!=0:  # number=0 should return the solution, in this case else will be skipped and the function will go to the if number==0 directly
			print("debug 18")
			return Cases
		else: 
			return Best
	else:# NO replacement solution was found, we have to scale up

		Indecies=[Nodes.index(n) for n in Best.Nodes]
		NewCases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,TempCase,ProbMat,TestMat,NodeObjects,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Cs,Ca,
									Indecies,0,start,Continue,
									Reason="Sat",Add="D") 
		Cases+=NewCases
		Best=GetBestWithFitness(Cases,SaturationPerPod,VProximity, Ep0, TempCase, NodeObjects,Cs,Ca,fitnessAlpha)
		if Best.Ep<Ep0: # if a solution was found without adding S node then don't test S nodes (more Expensive) 
			if number!=0:  # number=0 should return the solution, in this case else will be skipped and the function will go to the if number==0 directly
				print("debug 19")
				return Cases
			else: 
				return Best

		else: 
			NewCases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,TempCase,ProbMat,TestMat,NodeObjects,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Cs,Ca,
									Indecies,0,start,Continue,
									Reason="Sat",Add="S") 
			Cases+=NewCases

	if len(Cases)!=0: 
		Best=GetBestWithFitness(Cases,SaturationPerPod,VProximity, Ep0, TempCase, NodeObjects,Cs,Ca,fitnessAlpha)
	else: 
		Best=TempCase

	if not TestCondition(Best,start,0,Ep0,SaturationPerPod,number)[0]: # is the any solutions in the tested Cases
		# if No solution is found in the cases, the we need to do a new loop iteration
		Cases+=FixProximityQoSFitness(Nodes,NodeObjects,Best.Nodes,start,Best,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,Cs,Ca,fitnessAlpha,VImbalance,SaturationPerPod,Timeout,number=number+1)
		Best=GetBestWithFitness(Cases,SaturationPerPod,VProximity, Ep0, TempCase, NodeObjects,Cs,Ca,fitnessAlpha)

	if number==0: # in case of the first Recursion return the solution and not the cases
		for case in Cases: 
	 		print(case.Ep,case.Cost,case.Nodes)
		Best=GetBestWithFitness(Cases,SaturationPerPod,VProximity, Ep0, TempCase, NodeObjects,Cs,Ca,fitnessAlpha) 
		return Best
	print("debug 20")
	return Cases




def CheckscaleDown(Current,ProbMat,LatMat,FullTestMat,Nodes,ListReq,TotalReq,SelectedNodes,L,Ep0,LPC,VProximity=99.9, VImbalance=5,SaturationPerPod=50,Timeout=1): 
	DownScale=3
	Cases=[]
	IdleGateways=[]
	Servers={}
	Indecies=[]
	ActiveNodes=hona.GetActiveNodes(LPC,3)
	lenlpc=len(LPC)
	last=sum(list(LPC[lenlpc-1]))
	pr=0
	if lenlpc>1:
		pr=sum(list(LPC[lenlpc-2]))
	if pr-last>SaturationPerPod/2 and pr>SaturationPerPod : 
		DownScale=len(Current.Nodes)-(TotalReq/SaturationPerPod)-3
	else: 
		DownScale=1
	# for i in ActiveNodes: 
	# 	print(Nodes[i],end=",")
	# print("")
	for i in range(len(Nodes)): 
		if i not in ActiveNodes: 
			IdleGateways.append(i)

	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	#DfPrint(TestMat)
	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
		Indecies.append(Index)
	Unique,UNodes=GetUnique(TestMat,Indecies,Servers,Nodes)
	#print(Unique,UNodes)
	newSelected=SelectedNodes
	i=0
	Number=0
	start=time.time()
	print("DownScale",DownScale)
	while i<DownScale:
		i+=1
		Cases=[]
		if len(newSelected)<=3: 
			break
		for node in newSelected:
			if Nodes.index(node) not in UNodes: 
				Number+=1
				newNodes=hona.ExcludeNode(newSelected,node) 
				TempCase=hona.CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,newNodes,L,SaturationPerPod)
				#print("Ep:",TempCase.Ep,"Vl0:",TempCase.Vl0,"Vc0",TempCase.Vc0,"MaxLPP:",TempCase.MaxLPP,"Total:",sum(TempCase.RPP),[node for node in SelectedNodes],TempCase.RPP)
				Cases.append(TempCase)

				#print(TempCase.Proximity,TempCase.MaxLPP)
				#print(TempCase.__dict__)
		if len(Cases)!=0: 
			Best=GetBest2(Cases,SaturationPerPod,VProximity)
			print(Best.Proximity,Best.MaxLPP)
			if hona.TestCondition(Best,start,VProximity,Ep0,SaturationPerPod,10):
				#print(Current.Proximity,Current.RPP) 
				print(Best.Ep,Best.RPP)
				#print("reach!")
				Current=Best
				newSelected=Best.Nodes
			else: 
				break
		else: 
			break
	return Current

def CheckscaleDownWithS(Initial,Current,NodeObjects,ProbMat,LatMat,FullTestMat,Nodes,ListReq,TotalReq,SelectedNodes,L,Ep0,Cs,Ca,LPC,VProximity=99.9, VImbalance=5,SaturationPerPod=50,Timeout=1,Number=0,DownScale=0):
	print("CheckscaleDownWithS Recursion " + str(Number))
	Cases=[]
	Initial.Cost=CalculateCost(Initial,Initial,NodeObjects,Cs,Ca)
	IdleGateways=[]
	Servers={}
	Indecies=[]
	ActiveNodes=hona.GetActiveNodes(LPC,3)
	lenlpc=len(LPC)
	last=sum(list(LPC[lenlpc-1]))
	pr=0
	if Number==0: 
		if lenlpc>1:
			pr=sum(list(LPC[lenlpc-2]))
		if pr-last>SaturationPerPod/2 and pr>SaturationPerPod : 
			DownScale=len(Current.Nodes)-(TotalReq/SaturationPerPod)-3
		else: 
			DownScale=1
		# for i in ActiveNodes: 
	# 	print(Nodes[i],end=",")
	# print("")
	print("DOWNSCALE " + str(DownScale))
	for i in range(len(Nodes)): 
		if i not in ActiveNodes: 
			IdleGateways.append(i)
	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	Cases+=TestReplaceSaturation(Nodes,NodeObjects,FullTestMat,SelectedNodes,Current,ListReq,ProbMat,LatMat,TestMat,TotalReq,L,SaturationPerPod,Ep0,Cs,Ca,True,Replace="S",With="D")
	if len(Cases)!=0: 
		Best1=GetBest2(Cases,SaturationPerPod,VProximity)
	else: 
		Best1=Current
	SelectedNodes=Best1.Nodes
	if Number>DownScale+1: 
		return Best1
	#DfPrint(TestMat)
	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
		Indecies.append(Index)
	Unique,UNodes=GetUnique(TestMat,Indecies,Servers,Nodes)
	#print(Unique,UNodes)
	newSelected=SelectedNodes
	i=0
	start=time.time()
	print("TRY REMOVING S")
	Solutions=[]
	for node in newSelected:
		if Nodes.index(node) not in UNodes and GetNodeType(node,NodeObjects)=="S": 
			newNodes=hona.ExcludeNode(newSelected,node) 
			TempCase=hona.CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,newNodes,L,SaturationPerPod)
			TempCase.Cost=CalculateCost(Initial,TempCase,NodeObjects,Cs,Ca)
			Cases.append(TempCase)
			#print("Ep:",TempCase.Ep,"Vl0:",TempCase.Vl0,"Vc0",TempCase.Vc0,"MaxLPP:",TempCase.MaxLPP,"Total:",sum(TempCase.RPP),[node for node in SelectedNodes],TempCase.RPP)
			if hona.TestCondition(TempCase,start,0,Ep0,SaturationPerPod,2)[0]:
				Solutions+=[TempCase]
	if Solutions!=[]:
		print("S Removed")
		return CheckscaleDownWithS(Initial,Solutions[0],NodeObjects,ProbMat,LatMat,FullTestMat,Nodes,ListReq,TotalReq,Solutions[0].Nodes,L,Ep0,Cs,Ca,LPC,VProximity=99.9, VImbalance=5,SaturationPerPod=50,Timeout=1,Number=Number+1,DownScale=DownScale)
	# 	Best=GetBestWithCost(Cases,SaturationPerPod,VProximity,Ep0, 0, NodeObjects)
	#print(Ep0,Best.Ep, Best.Nodes== Best1.Nodes)
	#if Best.Ep<Ep0:
	#	print("S Removed")
	#	return CheckscaleDownWithS(Initial,Best,NodeObjects,ProbMat,LatMat,FullTestMat,Nodes,ListReq,TotalReq,Best.Nodes,L,Ep0,LPC,VProximity=99.9, VImbalance=5,SaturationPerPod=50,Timeout=1,Number=Number+1)
	else:
		print("TRY REMOVING D")
		for node in newSelected:
			if Nodes.index(node) not in UNodes and GetNodeType(node,NodeObjects)=="D": 
				newNodes=hona.ExcludeNode(newSelected,node) 
				TempCase=hona.CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,newNodes,L,SaturationPerPod)
				TempCase.Cost=CalculateCost(Initial,TempCase,NodeObjects,Cs,Ca)
				Cases.append(TempCase)
				#print("Ep:",TempCase.Ep,"Vl0:",TempCase.Vl0,"Vc0",TempCase.Vc0,"MaxLPP:",TempCase.MaxLPP,"Cost",TempCase.Cost,[node for node in SelectedNodes],TempCase.RPP)
				if hona.TestCondition(TempCase,start,0,Ep0,SaturationPerPod,2)[0]:
					Solutions+=[TempCase]
		# if len(Cases)==0:
		# 	Best=Best1
		# else: 
		# 	Best=GetBestWithCost(Cases,SaturationPerPod,VProximity,Ep0, 0, NodeObjects)
		# if Best.Ep<Ep0:
		if Solutions!=[]:
			print("D Removed") 
			return CheckscaleDownWithS(Initial,Solutions[0],NodeObjects,ProbMat,LatMat,FullTestMat,Nodes,ListReq,TotalReq,Solutions[0].Nodes,L,Ep0,Cs,Ca,LPC,VProximity=99.9, VImbalance=5,SaturationPerPod=50,Timeout=1,Number=Number+1,DownScale=DownScale)
	
		else: 
			print("Reach")
			return Best1	
