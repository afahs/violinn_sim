#!/usr/bin/python3

####STD LIBS#######
import math
###################

def CalculateADistance(n1,n2):
	"""
	Returns the latency between two nodes according to their Vivaldi Coordinates
	"""
	# Computes Normal Magnitude then adds the heights
	dist=CalacuteMagnitude(CoordinatesOperation(n1.vec,"-",vec2=n2.vec)) +n1.h +n2.h
	ADistance= dist + n1.adj + n2.adj #Then Adjust according to updated Vivaldi Algo   
	if ADistance>0:
		dist=ADistance
	assert dist> 0, "negative latency is produced"
	return dist*1000

def CalacuteMagnitude(vec):
	"""
	Computes the Magnitude of Vector vec
	"""
	s=0
	for x in vec: 
		s+=x*x
	return math.sqrt(s)

def CoordinatesOperation(vec1,Operation,vec2=None,factor=1):
	"""
	Returns a value for a vector operation
	"""
	assert len(vec1)==len(vec2) or Operation!="*", "The two vectors have different Dimensions"
	ret= [0 for i in range(len(vec1))] # the result vector

	if Operation=="+" and vec2!=None:
		for i in range(len(vec1)):
			ret[i]= vec1[i]+vec2[i]
	
	if Operation=="-":
		for i in range(len(vec1)):
			ret[i]= vec1[i]-vec2[i]

	if Operation=="*":
		for i in range(len(vec1)):
			ret[i]= vec1[i]*factor
	
	return ret
