#!/usr/bin/python3

####STD LIBS#######
import pickle as pk
import numpy as np
import pandas as pd
import math
import itertools
import statistics
import random
####### LIBS#######
###################

##################################TESTING FUNCS #############################
def DfPrint(df):
	print(pd.DataFrame(df))

import time
def Gettime(start,phrase):
	current=time.time()
	print(current-start,phrase)
	return current

def GetSpareNodes(NodeObjects): 
	Spare=[]
	for node in NodeObjects: 
		if node.type=="S": 
			Spare.append(node.name)
	return Spare


def mainfunc(L,Replicas,VProximity,VImbalance,SaturationPerPod,Timeout): # Testing function (TO BE REMOVED)
	latencies=PickleLoad("lat.pk")
	NodeObjects=PickleLoad("node.pk")
	Nodes=GetSortedNodes(latencies)
	Spare=GetSpareNodes(NodeObjects)
	LatMat=GetLatMat(latencies,Nodes)
	#DfPrint(LatMat)
	ProbMat=GetProbMat(latencies,Nodes,B=1,index=1)
	ListReq,TotalReq=CreateListReq(Nodes,NodeObjects)
	#start=time.time()
	SelectedNodes,TestMat,FullTestMat=GetMinReplicasNew(ProbMat,LatMat,Nodes,Spare,SaturationPerPod,ListReq,TotalReq,L)
	Best,Len=MinPlacementTesting(VProximity,VImbalance,SaturationPerPod,Timeout,ProbMat,TestMat,FullTestMat,LatMat,Nodes,Spare,ListReq,TotalReq,SelectedNodes,L)

	print("New Algo")
	print("selected solution", Best.Nodes)
	print("Number of Tested cases: ", Len)
	print(Best.Proximity,Best.Imbalance,Best.RPP)
	#start=Gettime(start,"GetMinReplicas")
	#Cases=StudyAllCases(ProbMat,LatMat,Nodes,ListReq,TotalReq,25,10)
	#Best=GetBest(Cases, a=0.97)
	#print(Best.__dict__)
	
	#return GetPlacement(Nodes,ProbMat,LatMat,ListReq,TotalReq,Replicas,L)
	#start=Gettime(start,"Algo")
	return Best

def mainfunc2(L,Replicas): # Testing function (TO BE REMOVED)
	latencies=PickleLoad("lat.pk")
	NodeObjects=PickleLoad("node.pk")
	Nodes=GetSortedNodes(latencies)
	LatMat=GetLatMat(latencies,Nodes)
	#DfPrint(LatMat)
	ProbMat=GetProbMat(latencies,Nodes,B=1,index=1)
	ListReq,TotalReq=CreateListReq(Nodes,NodeObjects)
	#start=time.time()
	#start=Gettime(start,"GetMinReplicas")
	#Cases=StudyAllCases(ProbMat,LatMat,Nodes,ListReq,TotalReq,25,10)
	#Best=GetBest(Cases, a=0.97)
	#print(Best.__dict__)
	
	return GetPlacement(Nodes,ProbMat,LatMat,ListReq,TotalReq,Replicas,L)
	#start=Gettime(start,"Algo")
	return Allocate

#############################################################################

def PickleLoad(filename):
	"""
	Load the saved pk files
	"""
	pickledlist=[]
	with open("./temp/"+filename, 'rb') as f:
		pickledlist = pk.load(f)
	return pickledlist

def GetLatency(Node1,Node2,latencies):
	"""
	Returns the latency between Node1 and Node2
	"""
	for k,v in latencies[Node1]: 
		if k==Node2: 
			return v 
	print("latency not found, Returning 0")
	return 0

def GetSortedNodes(latencies):
	"""
	Returns a list of Sorted Nodes
	"""
	Nodes=[]
	for k,v in latencies.items():
		Nodes.append(k)
	Nodes=sorted(Nodes)
	return Nodes

def GetLatMat(latencies,Nodes):
	"""
	Create a Latency ma as np array
	""" 
	l=[]
	for node1 in Nodes:
		temp=[]
		for node2 in Nodes:
			temp.append(GetLatency(node1,node2,latencies))
		l.append(temp)
	LatMat=np.array(l)
	return LatMat

def GetProbMat(latencies,Nodes,B=1,index=1):
	"""
	Create a unified Probability that will be used with all the selected placement: 
		latencies 	=> Inter-node Latencies DICT
		Nodes 		=> list of SORTED nodes names LIST OF STRINGS
		B 			=> The value Beta in Proxy-mity equation FLOAT ]0,1]
		index		=> Decides what function to be used to calculate function F INT {1,2,3}
	"""
	ProbMat=[]
	for node1 in Nodes: 
		temp=[]
		sumF=0
		for node2 in Nodes: 
			l=GetLatency(node1,node2,latencies)
			#Apply proxy-mity Routing function
			F=CalculateF(l,B=B,index=index)
			temp.append(F)
			sumF+=F 
		for node2 in Nodes: 
			temp[Nodes.index(node2)]=temp[Nodes.index(node2)]/sumF
		ProbMat.append(temp)
	ProbMat=np.array(ProbMat)
	return ProbMat

def CalculateF(x,B=1,index=1):
	"""
	Calculate the function defined in Proxy-mity:
		x			=> The latency between the two tested nodes
		B 			=> The value Beta in Proxy-mity equation FLOAT ]0,1]
		index		=> Decides what function to be used to calculate function F INT {1,2,3}

	"""
	assert x>0, "the latency value (%r) is negative or zero" % x
	if index==1: # Choose the Exponential function
		return math.exp(-B*x)
	if index==2: # Choose the proportional function
		return 1/(B*x)
	if index==3: # Choose the Degree function
		return 1/math.pow(x,B)

def CreateListReq(Nodes,NodeObjects): 
	"""
	Returns the number of Received request for each node
	and the total number of requests
	"""
	assert len(Nodes)==len(NodeObjects), "list of nodes is not equal to NodeObject" 
	#If this assert is issued that means that the nodes collected by Latencies(Serf) are different than those
	# of Node.pk (kubectl)=> serf have failed in a node or a node have failed. 
	ListReq=[0 for i in range(len(Nodes))]
	TotalReq=0
	for node in NodeObjects:
		assert node.name in Nodes, node.name + " is Node available in Nodes"
		if not node.Idle: 
			ListReq[Nodes.index(node.name)]=node.req
			TotalReq+=node.req
	return ListReq,TotalReq

class Case:
	""" 
	class of placement cases that are being studied: 
		id			=> The sequential number of the case INT
		Nodes 		=> The Selected Nodes for the placement LIST OF OBJECTS NODE
		Proximity 	=> The Proximity Variable of the Objective function, Describes the tail latency FLOAT IN %
		Imbalance	=> The Imbalance Variable of the Objective function, Describes the load balancing FLOAT IN %
		RPP			=> The Expected load per pod ARRAY OF FLOAT
		Omega		=> The Objective function result to be maximized FLOAR ]0,1]
	""" 
	def __init__(self,Proximity,Imbalance,SelectedNodes,ProbMat,Number,RPP,Vc0,Vl0):
		self.id    		= Number
		self.Nodes 		= SelectedNodes
		self.Proximity	= Proximity
		self.Imbalance	= Imbalance
		self.RPP		= RPP
		self.MaxLPP		= max(RPP)
		self.MeanLPP	= statistics.mean(RPP)
		self.Omega		= 0
		self.ProbMat    = ProbMat
		self.Slow 		= []
		self.BS 		= 0
		if sum(RPP)==0: 
			self.Ep 		= 0
		else: 
			self.Ep 		= (Vl0+Vc0)*100/sum(RPP)
		self.Vc0		= Vc0
		self.Vl0		= Vl0
		self.Cost 		= 0
		self.Fitness		= 0

def StudyAllCases(ProbMat,LatMat,Nodes,ListReq,TotalReq,L,Replicas):
	"""
	Computes the optimal solution(Brute Force): 
		ProbMat 	=> Unified probability matrix NP ARRAY
		LatMat 		=> Latency matrix NP ARRAY
		Nodes 		=> List of sorted Nodes LIST OF STRINGS
		ListReq 	=> List of requested per node (end-user Load) Sorted according to the list of nodes ARRAY OF INT
		TotalReq 	=> Total Number of end-user requests INT
		L 			=> Threshold latency defined by Hona, any latency above L will be considered slow FLOAT 
		Replicas	=> Number of replicas to be placed. 
	"""
	cases=[]
	placements=itertools.combinations(Nodes,Replicas)
	for p in placements: 
		SelectedNodes=p
		Proximity,Imbalance,Temp,RPP=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L)
		cases.append(Case(Proximity,Imbalance,SelectedNodes,Temp,0,RPP))
		#print(p,Proximity,Imbalance)
	return cases

def CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,C0,Number=0): 
	"""
	Calculates the variable Proximity (P%) and Imbalance(I%) for a placement
	represented by the SelectedNodes
	"""
	Temp=[]
	Temp= CreateCaseMatrix(SelectedNodes,Nodes,ProbMat) #ProbMat for the specific Placement
	slowReq	=	0	# Number of Slow Requests (Latency> L)
	RPP		=	[]	# Estimated Requests Per Pod
	assert len(SelectedNodes)==len(Temp[0]),"Inconsistent Temp size" # mostly a duplicate of the same node
	for i in range(len(SelectedNodes)):
		IND=Nodes.index(SelectedNodes[i])
		Total=0
		for j in range(len(Nodes)):
			assert Temp[j,i] > 0, "A probability is negative(%r), ProbMat is Wrong" % Temp[j,i]
			Req=Temp[j,i]*ListReq[j]
			Total+=Req
			if LatMat[j,IND] > L: 
				slowReq+=Req

		RPP.append(Total)
	Proximity= (1-(slowReq/TotalReq))*100 #in percent 
	std=statistics.stdev(RPP)
	Imbalance=(std/TotalReq)*100 # in percent
	Vc0=0
	for p in RPP: 
		if p > C0: 
			Vc0+=p-C0

	Casetemp=Case(Proximity,Imbalance,SelectedNodes,Temp,Number,RPP,Vc0,slowReq)
	return Casetemp

def CalculateSlowPerGateway(Case,LatMat,Nodes,SelectedNodes,Temp,L,ListReq): 
	Slow={}
	for i in range(len(Nodes)):
		Slow[i]=0
	for i in range(len(Nodes)):
		for j in range(len(SelectedNodes)):
			Req=Temp[i,j]*ListReq[i]
			IND=Nodes.index(SelectedNodes[j])
			if LatMat[i,IND] > L:
				Slow[i]+=Req
	Slow={k:v for k,v in Slow.items() if v!=0}
	Slow={k:v for k,v in sorted(Slow.items(), key=lambda item: item[1], reverse=True)}

	return Slow

def CreateCaseMatrix(SelectedNodes,Nodes,ProbMat): # should be sorted Nodes and sorted SelectedNodes
	"""
	Create a new ProbMat from the unified one
	The new ProbMat illustrate the probability distribution for a single placement
	SelectedNodes SHOULD BE SORTED ACCORDING TO NODES LIST
	"""
	assert CheckList(Nodes,SelectedNodes), "Not all of the selected nodes are found in the list of nodes"
	# Remove the probabilities of the nodes that does not have a server according to the tested placement.
	assert len(ProbMat)>0, "ProbMat is an empty Matrix"
	# An error occured in the calculation of ProbMat
	# Remove the nodes that are not Selected from ProbMat
	Temp=MakeColRemove(ProbMat,Nodes,SelectedNodes)
	# Normalize
	Temp /=  Temp.sum(axis=1)[:,np.newaxis]
	return Temp

def CheckList(Nodes,SelectedNodes): 
	"""
	Checks if all the SelectedNodes are found in Nodes
	Error checking function 
	"""
	for node in SelectedNodes: 
		if node not in Nodes:
			print("Not Found"+node)
			return False
	return True

def CheckPlacemnet(TestMat, SelectedNodes):
	# Temp=TestMat[:,SelectedNodes] # find something faster DONE
	# y=np.sum(TestMat,axis=1)
	# print(y)
	temp=TestMat[:,SelectedNodes]
	y=np.sum(temp,axis=1)
	if 0 in y: 
		return False
	return True
	# for row in range(len(TestMat)):
	# 	Sum=0
	# 	for i in SelectedNodes:
	# 		Sum+=TestMat[row,i]
	# 	if Sum==0:
	# 		return False
	# return True
def MakeColRemove(ProbMat,Nodes,SelectedNodes):
	"""
	Remove a column from the ProbMat
	those columns represent the nodes that are not selected
	thus the probability is equal to zero
	"""
	NIND=[]
	for i in range(len(Nodes)): 
		if Nodes[i] not in SelectedNodes: 
			NIND.append(i)
	temp=np.delete(ProbMat,NIND,axis=1)
	return temp

# def GetBest(Cases,SaturationPerPod, a=0.97):
# 	"""
# 	Compute the objective function result OMEGA
# 	and then returns the case with the Max value of Omega (Best Case)
# 		Cases 	=> A list of objects Case
# 		a 		=> The value Alpha defined by Hona to control the trade-off 
# 				   between Proximity and Imbalance
# 	"""
# 	Print("This function was called")
# 	assert True, "Getbest1 was called"
# 	NewCases=[]
# 	for case in Cases: 
# 		if case.MaxLPP<SaturationPerPod: 
# 			NewCases.append(case)
# 	if len(NewCases)!=0: 
# 		Cases=NewCases
# 	Min,Max=GetMinMax(Cases)
# 	for c in Cases:
# 		c.Omega=a*(c.Proximity/Max)+(1-a)*(Min/c.Imbalance)
# 	return GetBestD(Cases)

# def GetBestD(Cases):
# 	"""
# 	Returns the case with the Max value of Omega
# 	"""
# 	best=Cases[0] 
# 	for c in Cases: 
# 		if c.Omega>best.Omega: 
# 			best=c
# 	return best

# def GetMinMax(Cases):
# 	"""
# 	Returns the Min value of Imbalance in the studied cases
# 	And the Max value of Proximity
# 	"""
# 	Max=Cases[0].Proximity
# 	Min=Cases[0].Imbalance 
# 	for c in Cases: 
# 		if c.Imbalance<Min:
# 			Min=c.Imbalance 
# 		if c.Proximity>Max:
# 			Max=c.Proximity
# 	return Min,Max

# def GetMinMax2(Cases):
# 	"""
# 	Returns the Min value of Imbalance in the studied cases
# 	And the Max value of Proximity
# 	"""
# 	Max=Cases[0].Proximity
# 	Min=Cases[0].MaxLPP 
# 	for c in Cases: 
# 		if c.MaxLPP<Min:
# 			Min=c.MaxLPP 
# 		if c.Proximity>Max:
# 			Max=c.Proximity
# 	return Min,Max
def ChechforSpare(Case,Spare): 
	for node in Case.Nodes: 
		if node in Spare: 
			return False
	return True

def GetBestInit(Cases,SaturationPerPod,VProximity,Spare,Nodes):
	"""
	Compute the objective function result OMEGA
	and then returns the case with the Max value of Omega (Best Case)
		Cases 	=> A list of objects Case
		a 		=> The value Alpha defined by Hona to control the trade-off 
				   between Proximity and Imbalance
	"""
	MinEp=100
	BestCase=Cases[0]
	for case in Cases:
		case.Vl0=sum(case.RPP)*(100-case.Proximity)/100
		case.Vc0=0
		for p in case.RPP: 
			if p > SaturationPerPod: 
				case.Vc0+=p-SaturationPerPod
		case.Ep=(case.Vc0+case.Vl0)*100/sum(case.RPP)
		#print("Vl0",case.Vc0,"Vc0",case.Vc0,"Ep",case.Ep,"total",sum(case.RPP),case.Nodes)
		if case.Ep<MinEp and ChechforSpare(case,Spare): 
			MinEp=case.Ep
			BestCase=case
	return BestCase

def GetBest2(Cases,SaturationPerPod,VProximity):
	"""
	Compute the objective function result OMEGA
	and then returns the case with the Max value of Omega (Best Case)
		Cases 	=> A list of objects Case
		a 		=> The value Alpha defined by Hona to control the trade-off 
				   between Proximity and Imbalance
	"""
	MinEp=100
	BestCase=Cases[0]
	for case in Cases:
		case.Vl0=sum(case.RPP)*(100-case.Proximity)/100
		case.Vc0=0
		for p in case.RPP: 
			if p > SaturationPerPod: 
				case.Vc0+=p-SaturationPerPod
		case.Ep=(case.Vc0+case.Vl0)*100/sum(case.RPP)
		#print("Vl0",case.Vc0,"Vc0",case.Vc0,"Ep",case.Ep,"total",sum(case.RPP),case.Nodes)
		if case.Ep<MinEp: 
			MinEp=case.Ep
			BestCase=case
	return BestCase

def GetPercentages(ListReq,TotalReq):
	"""
	Create the weight per node list, load received from end users
	"""
	assert TotalReq>0, "TotalReq=0"
	Weight=np.true_divide(ListReq,TotalReq)
	return Weight

def CreateTestMat(LatMat,L):
	"""
	Create a Bool matrix of the latencies such that each latency>L will be false
	and True otherwise
	"""
	Lenlatmat=len(LatMat)
	temp=np.zeros([Lenlatmat,Lenlatmat], dtype=np.bool)
	for i in range(len(temp)):
		for j in range(len(temp[0])):
			if LatMat[i,j]>=L:
				temp[i,j]=False
			else:
				temp[i,j]=True
	return temp

def GetPlacement(Nodes,ProbMat,LatMat,ListReq,TotalReq,Replicas,L):
	"""
	A function that Returns the chosen placement:
		ProbMat 	=> Unified probability matrix NP ARRAY
		LatMat 		=> Latency matrix NP ARRAY
		Nodes 		=> List of sorted Nodes LIST OF STRINGS
		ListReq 	=> List of requested per node (end-user Load) Sorted according to the list of nodes ARRAY OF INT
		TotalReq 	=> Total Number of end-user requests INT
		L 			=> Threshold latency defined by Hona, any latency above L will be considered slow FLOAT 
		Replicas	=> Number of replicas to be placed
	"""
	P=2 # Determine the Window size
	Weight=GetPercentages(ListReq,TotalReq) # calculate weight per gateway
	Weight=Weight.round(2) #round the weight
	TestMat=CreateTestMat(LatMat,L)
	#DfPrint(TestMat)
	#print(Weight)
	Temp = np.diag(Weight)
	TestMat=np.dot(Temp,TestMat) # Multiply the TestMat by the weight of each gateway
	#DfPrint(TestMat)
	Weight=Weight[Weight!=0]
	y=np.sum(TestMat,axis=1)
	NIND=[]
	for i in range(len(y)): # remove gateway that does not produce load
		if y[i]==0:
			NIND.append(i)
	TestMat=np.delete(TestMat,NIND,axis=0)
	y=np.delete(y,NIND)
	#TestMat=TestMat.round(2)
	#start=time.time()
	#DfPrint(TestMat)
	Servers=[]
	IsolatedNodes=[]
	#print(Weight)
	for i in range(len(TestMat[0])): # find something faster
		v=sum(TestMat[:,i])
		if v!=0:
			Servers.append((i,v))
	for i in range(len(y)):
		if y[i]==Weight[i]:
			IsolatedNodes.append(int(np.where(TestMat[i]!=0)[0]))
	#print("Isolated Nodes:",end=" ")
	# for i in IsolatedNodes:
	# 	print(Nodes[i],end=";")
	# print("")
	Servers=sorted(Servers, key=lambda x: x[1],reverse=True)
	Servers=Servers[0:Replicas*P]# fix
	best=TestSelectedNodes(Nodes,ProbMat,LatMat,TestMat,Servers,IsolatedNodes,ListReq,TotalReq,Replicas,P,L)
	return best

def TestSelectedNodes(Nodes,ProbMat,LatMat,TestMat,Servers,IsolatedNodes,ListReq,TotalReq,Replicas,P,L):
	"""
	A function that Returns the chosen placement:
		ProbMat 	=> Unified probability matrix NP ARRAY
		LatMat 		=> Latency matrix NP ARRAY
		Nodes 		=> List of sorted Nodes LIST OF STRINGS
		ListReq 	=> List of requested per node (end-user Load) Sorted according to the list of nodes ARRAY OF INT
		TotalReq 	=> Total Number of end-user requests INT
		IsolatedN 	=> List of Nodes with only one fast link
		Servers 	=> List of Possible Server node and their computed cumulative weight
		L 			=> Threshold latency defined by Hona, any latency above L will be considered slow FLOATq
		Replicas	=> Number of replicas to be placed
	"""
	assert len(IsolatedNodes)< Replicas, "The IsolatedNodes are greater than the number of replicas"
	#print(Servers)
	SelectedNodes=[]
	TestedPlacements=[]
	if IsolatedNodes!=[]:
		ToBeSelected=Replicas-len(IsolatedNodes)
	else:
		ToBeSelected=Replicas
	i=0
	k=0
	Cases=[]
	print(IsolatedNodes)
	while True: # IMPLEMENT A CONDITION CHECK FUNCTION
		TempNodes=[]
		for node in IsolatedNodes:
			TempNodes.append(node)
		if i==0:
			for j in range(ToBeSelected):
				TempNodes.append(Servers[j][0])

		else:
			Window=math.floor(i/5)+ToBeSelected +1
			if Window> Replicas*P: # change
				Window=Replicas*P
			Set= [j for j,k in Servers[0:Window]]
			RandomSubet=random.sample(Set,ToBeSelected)
			for node in RandomSubet:
				TempNodes.append(node)
		SelectedNodes=[Nodes[j] for j in sorted(TempNodes)]

		if CheckPlacemnet(TestMat, TempNodes)==True and SelectedNodes not in TestedPlacements:
		#if  SelectedNodes not in TestedPlacements:
			k+=1
			TestedPlacements.append(SelectedNodes)
			Proximity,Imbalance,Temp,RPP=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L)
			Cases.append(Case(Proximity,Imbalance,SelectedNodes,Temp,k,RPP))
			#print(Proximity,Imbalance)
		if k==10:
			#print("Kreached")
			break
		i+=1
		#print(k)
		if i>1000 and k!=0 :
			#print("ireached")
			break
	# for c in Cases:
	# 	print(c.id,c.Proximity,c.Imbalance,c.Nodes)
	best=GetBest(Cases)
	return best

def GetMinReplicas(ProbMat,LatMat,Nodes,Spare,ListReq,TotalReq,L,C0):
	Allocate=[]
	TestMat=CreateTestMat(LatMat,L) # Create the Mat the indicates the fast link in the Network
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	#print(len(TestMat))
	while len(TNodes)!= 0:
		y=np.sum(TestMat,axis=1) # How many Fast link each node have
		ToBeAllocated=TNodes[0] # select the first node in the list (TBA)
		print("y",y)
		if y[0]==1: 
			Allocate.append(Nodes.index(TNodes[0]))
			TNodes=np.delete(TNodes,[0])
			TestMat=TrimTestMat(TestMat,[0])
			print("Allocated",Allocate)
			continue
		Possible={} # a Dict that describes how many fast link each node of TBA nearby Nodes have 
		Max=0
		for i in range(len(TestMat)): 
			if TestMat[0,i]==True:
				v=y[i]
				if v>Max: #select the Node\Nodes that have the highest number of Fast links
					Max=v
					Possible={}
					Possible[i]=v
				else:
					if v==Max: 
						Possible[i]=v
		if len(Possible)!=1:# if more than one Node have the same number of Fast links
			i=0
			Temp={}
			for k,v in Possible.items():
				v2=GetSecondLayerValues(k,TestMat,y) 
				if i==0: 
					Min=v2
					Temp[k]=(v,Min)
					i=1
				if v2<Min: # choose the one that contain badly placed node
					Temp={}
					Temp[k]=(v,v2)
			Possible=Temp
		k=list(Possible.keys())[0]
		print("Possible",Possible)
		assert len(Possible)==1, "The length of the Possible Solutions didn't reach one"
		Allocate.append(Nodes.index(TNodes[k]))
		NIND=[]
		for i in range(len(TestMat)): 
			if TestMat[k,i]==True:
				NIND.append(i)
		#print(TNodes)
		#print(pd.DataFrame(TestMat))
		TNodes=np.delete(TNodes,NIND)#Remove the nodes that got a nearby Node
		TestMat=TrimTestMat(TestMat,NIND)#Minimize the TestMat
		print("Allocated",Allocate)
		#print(TNodes)
		#print(pd.DataFrame(TestMat))
	SelectedNodes=[Nodes[j] for j in sorted(Allocate)]
	print(SelectedNodes)
	Proximity,Imbalance,Temp,RPP=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,C0)
	print(Proximity,Imbalance)
	return SelectedNodes


def TrimTestMat(TestMat,NIND): 
	temp=np.delete(TestMat,NIND,axis=1)
	temp=np.delete(temp,NIND,axis=0)
	return temp


def GetSecondLayerValues(Index,TestMat,y):# to Test How each not that is covered by placement Index is covered
	temp=[]
	for i in range(len(TestMat)): 
		if TestMat[Index,i]==True:
			temp.append(y[i])
	return sum(temp)

def GetPotentialNodes(SpareIndecies,y): 
	dicty = { i : y[i] for i in range(len(y)) }
	dicty = sorted(dicty.items(), key=lambda kv: kv[1],reverse=True)
	for k,v in dicty: 
		if k not in SpareIndecies: 
			return k
	return dicty[0][0]


def GetMinReplicasNew(ProbMat,LatMat,Nodes,Spare,SaturationPerPod,ListReq,TotalReq,L):
	Allocate=[]
	FullTestMat=CreateTestMat(LatMat,L) # Create the Mat the indicates the fast link in the Network
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	IdleGateways=[]
	SpareIndecies=[Nodes.index(node) for node in Spare]
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)

	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	#DfPrint(TestMat)
	while len(TestMat)!= 0:
		y=np.sum(TestMat,axis=0) # How many Fast link each Server have
		ToBeAllocated=TNodes[0] # select the first node in the list (TBA)
		Potential=GetPotentialNodes(SpareIndecies,y)
		# Potential=np.where(y==max(y))[0]
		# print(Potential)
		# Potential=Potential[0]
		Allocate.append(Potential)

		NIND=[]
		for b in range(len(TestMat)):
			if TestMat[b,Potential]:
				NIND.append(b)
		TestMat=np.delete(TestMat,NIND,axis=0)

	TestMat=CreateTestMat(LatMat,L)
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)
	TestMat=np.delete(TestMat,IdleGateways,axis=0)
	SelectedNodes=[Nodes[j] for j in sorted(Allocate)]
	Servers={}

	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
	Unique,NewIndecies=GetUnique(TestMat,Allocate,Servers,Nodes)
	NumberOfPods=math.ceil(TotalReq/SaturationPerPod) +1
	LinkPerServer=[]
	assert NumberOfPods<len(Nodes),"unrealistic saturation value" # we cannot create pods more than the Nodes
	if NumberOfPods>len(NewIndecies):
		diff=NumberOfPods-len(NewIndecies)
		print("Scaling up by %s to meet saturation constraints"% diff)
		SumList=TestMat.sum(axis=0)
		for i in range(len(SumList)): 
			LinkPerServer.append((i,SumList[i]))
		#print(NewIndecies)
		Remove=[]
		for item in LinkPerServer: 
			if item[0] in NewIndecies: 
				Remove.append(item)
		for item in Remove: 
			LinkPerServer.remove(item)
		LinkPerServer.sort(key=lambda tup: tup[1], reverse=True)
		for i in range(diff): 
			NewIndecies.append(LinkPerServer[i][0])

	SelectedNodes=[Nodes[j] for j in sorted(NewIndecies)]
	return SelectedNodes,TestMat,FullTestMat


def MinPlacementTesting(VProximity,VImbalance,SaturationPerPod,Timeout,ProbMat,TestMat,FullTestMat,LatMat,Nodes,Spare,ListReq,TotalReq,SelectedNodes,L):
	Indecies=[]
	Servers={}
	Cases=[]
	NewCases=[]
	start=time.time()
	Number=0
	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
	TempCase=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,SaturationPerPod,Number=0)
	Cases.append(TempCase)
	if VProximity>TempCase.Proximity:
		NewCases=[]
		Slow=CalculateSlowPerGateway(Case,LatMat,Nodes,SelectedNodes,TempCase.ProbMat,L,ListReq)
		NewCases,Number=FixInitialProximity(Nodes,SelectedNodes,start,
							Slow,TestMat,FullTestMat,
							Servers,ProbMat,LatMat,
							ListReq,TotalReq,L,Number,
							VProximity,VImbalance,SaturationPerPod,
							Timeout)
		Cases=Cases+NewCases
		TempCase=GetBestInit(Cases,SaturationPerPod,VProximity,Spare,Nodes)
	# print("")
	# print("Temp")
	# print(TempCase.Proximity,TempCase.Imbalance,TempCase.MaxLPP,[Nodes.index(node) for node in SelectedNodes])
	# print("")
	if SaturationPerPod<TempCase.MaxLPP:
		NewCases=[]
		NewCases,Number=FixInitialSaturation(Nodes,SelectedNodes,start,
							TempCase,TestMat,FullTestMat,
							Servers,ProbMat,LatMat,
							ListReq,TotalReq,L,Number,
							VProximity,VImbalance,SaturationPerPod,
							Timeout)
		Cases=Cases+NewCases
		TempCase=GetBestInit(Cases,SaturationPerPod,VProximity,Spare,Nodes)
	print("")
	print("Temp")
	print(TempCase.Proximity,TempCase.Imbalance,TempCase.MaxLPP,[Nodes.index(node) for node in SelectedNodes])
	print("")
	NewCases=[]
	if TempCase.MaxLPP>SaturationPerPod or TempCase.Proximity<VProximity: 
		Indecies=[Nodes.index(n) for n in TempCase.Nodes]
		print("Scale up initialized")
		NewCases,Number,cond=AutoScale(VProximity,VImbalance,SaturationPerPod,
						Timeout,ProbMat,TestMat,
						LatMat,Nodes,ListReq,
						TotalReq,TempCase.Nodes,L,
						Indecies,Number,start,Reason="Sat")
		Cases=Cases+NewCases
	Best=GetBestInit(Cases,SaturationPerPod,VProximity,Spare,Nodes)
	for node in Best.Nodes:
		assert node not in Spare, "A spare node was selected in the initial placement, Aborting"
	print("Final placement Size",len(Best.Nodes))
	#print(Nodes)
	return Best,Number


def GetUnique(TestMat,Indecies,Servers,Nodes): # optimize
	temp=TestMat[:,Indecies]
	Unique=[]
	for i in range(len(Indecies)):
		li=[] 
		for j in range(len(temp)): 
			if temp[j,i]==True: 
				if sum(temp[j])==1: 
					li.append(j)
		if len(li)!=0: 
			Unique.append((Indecies[i],li,len(li)))# the unique nodes and the gateways that depends on them
	NewIndecies=[]
	NewIndecies=[item[0] for item in Unique] # the unique nodes 
	#NewIndecies=Indecies
	Unique.sort(key=lambda tup: tup[2])
	return Unique,NewIndecies 

def GetScores(Indecies,ToBeAssigned,FullTestMat,Unique,UNodes):
	# ADD=[]
	# for i in range(len(FullTestMat)): 
	# 	if i not in Indecies: 
	# 		ADD.append(i)
	scores=[]
	ToBeTested={}
	#print(len(ADD))
	NotUnique=[i for i in Indecies if i not in UNodes] # those nodes can be replaced without affecting the Proximity

	# if NotUnique!=[]:
	# 	for node in NotUnique: 

	temp=FullTestMat[ToBeAssigned,:] #ToBeAssigned are the nodes that have slow latency
	y=np.sum(temp,axis=0) # for every available gateway node, get how many fast link the gateway can provide for the TBA
	for i in range(len(y)):
		if i not in Indecies and y[i]!=0: # Selected nodes are excluded, and gateways with 0 fast links
			scores.append((i,y[i])) # A score for every possible gateway
	scores.sort(key=lambda tup: tup[1], reverse=True) 
	Newscores=[]
	for i in range(len(scores)): 
		if FullTestMat[ToBeAssigned[0],scores[i][0]] and i<11: # select only the first 10 nodes that can serve the first TBA 
			Newscores.append(scores[i][0])
	for i in NotUnique: # Try to replace an unimportant node by one of the nodes with high score
		ToBeTested[i]=Newscores
	return ToBeTested # returns the potential placement

def FixInitialProximity(Nodes,SelectedNodes,start,Slow,TestMat,FullTestMat,Servers,ProbMat,LatMat,ListReq,TotalReq,L,Number,VProximity,VImbalance,SaturationPerPod,Timeout): 
	Indecies=[Nodes.index(n) for n in SelectedNodes]
	#Slow=CalculateSlowPerGateway(Case,LatMat,Nodes,SelectedNodes,TempCase.ProbMat,L,ListReq)
	ToBeAssigned=[]
	i=0
	for k,v in Slow.items():
		if i> len(Slow)/5: 
			break
		ToBeAssigned.append(k)
		i+=1
	Unique,UNodes=GetUnique(TestMat,Indecies,Servers,Nodes)
	ToBeTested=GetScores(Indecies,ToBeAssigned,FullTestMat,Unique,UNodes)
	Cases=[]
	for key,li in ToBeTested.items():
		Indecies.remove(key)
		for i in li:
			#print(NewIndecies)
			Indecies.append(i)
			Indecies.sort()
			#print(NewIndecies)
			Number+=1
			SelectedNodes=[Nodes[i] for i in Indecies ]
			TempCase=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,SaturationPerPod,Number=Number)
			print(TempCase.Proximity,TempCase.Imbalance,TempCase.MaxLPP,[Nodes.index(node) for node in SelectedNodes])
			Cases.append(TempCase)
			if TestCondition(TempCase,start,VProximity,VImbalance,SaturationPerPod,Timeout)[0]: 
				return Cases,Number

			Indecies.remove(i)
		Indecies.append(key)
		Indecies.sort()
	return Cases,Number

def FixInitialSaturation(Nodes,SelectedNodes,start,TempCase,TestMat,FullTestMat,Servers,ProbMat,LatMat,ListReq,TotalReq,L,Number,VProximity,VImbalance,SaturationPerPod,Timeout): 
	RPPNode=[]
	ToBeTested={}
	Indecies=[Nodes.index(n) for n in TempCase.Nodes]
	for i in range(len(TempCase.Nodes)): 
		RPPNode.append((Nodes.index(TempCase.Nodes[i]),TempCase.RPP[i]))
	RPPNode.sort(key=lambda tup: tup[1])
	Unique,UNodes=GetUnique(TestMat,Indecies,Servers,Nodes)
	NexTo=RPPNode[len(RPPNode)-1][0]
	print(Unique)
	for i in range(len(RPPNode)-1): 
		ToBeReplaced=RPPNode[i][0]
		ProperNodes=GetProperNodes(ToBeReplaced,Indecies,Nodes,FullTestMat,NexTo,Unique,UNodes)
		ToBeTested[ToBeReplaced]=ProperNodes
	Cases=[]
	print(Indecies)
	Compare=TempCase
	for key,li in ToBeTested.items():
		#print("YOHOOOO")
		#print(key)
		Indecies.remove(key)

		for i in li:
			#print(key,i)
			#print(NewIndecies)
			Indecies.append(i)
			Indecies.sort()
			#print(NewIndecies)
			Number+=1
			#print(Indecies)
			SelectedNodes=[Nodes[i] for i in Indecies ]
			Temp=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,SaturationPerPod,Number=Number)
			print(Temp.Proximity,Temp.Imbalance,Temp.MaxLPP,[Nodes.index(node) for node in SelectedNodes])
			Cases.append(Temp)
			if TestCondition(Temp,start,VProximity,VImbalance,SaturationPerPod,Timeout)[0]: 
				return Cases,Number

			Indecies.remove(i)

		Indecies.append(key)
		Indecies.sort()
	return Cases,Number

def GetProperNodes(ToBeReplaced,Indecies,Nodes,FullTestMat,NexTo,Unique,UNodes):
	# IN(Isolated Nodes) is the nodes who have TBR as the only server
	ProperNodes=[]
	if ToBeReplaced in UNodes: 
		IN= [item for item in Unique if item[0]==ToBeReplaced][0][1]# IN(Isolated Nodes) is the nodes who have TBR as the only server
		temp=FullTestMat[IN,:]
		for i in range(len(temp[0])): 
			if i==ToBeReplaced: 
				continue
			Flag=True
			for j in range(len(temp)): 
				if not temp[j,i]:
					Flag=False
					break
			if Flag: 
				ProperNodes.append(i)
	else:
		#print(ToBeReplaced)
		ProperNodes=list(range(len(Nodes)))
		ProperNodes.remove(ToBeReplaced)
	NewProper=[]
	for node in ProperNodes: 
		if FullTestMat[NexTo,node] and node not in Indecies: 
			NewProper.append(node)
	return NewProper

def TestOtherCases(TestMat,NewIndecies,Servers,Nodes,Unique):	
	ToBeTested={}
	for item in Unique: 
		ToBeTested[item[0]]=[]
		for i in range(len(TestMat[0])): 
			test=0
			for j in item[1]:
				if TestMat[j,i]==False:
					test=1
					break
			if test==0:
				if i==item[0]: 
					continue 
				ToBeTested[item[0]].append(i)
	#print(ToBeTested)

	return ToBeTested

def TestCondition(Case,start,VProximity,Ep0,SaturationPerPod,timeout): 
	if Case.Ep<Ep0:#and Case.Imbalance<VImbalance:
		#print(Case.Nodes)
		#print("A solution was found")
		#print(Case.MaxLPP,Case.Proximity,Case.Nodes) 
		return True,False
	if time.time()-start>timeout:
		print("Timeout") 
		return True,True
	return False,False

def AutoScale(VProximity,VImbalance,SaturationPerPod,Timeout,ProbMat,TestMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,NewIndecies,Number,start,Reason="Sat"): 
	Upscale=1
	NewCases=[]
	#print(NewIndecies)
	for node in Nodes: 
		if Nodes.index(node) not in NewIndecies: 
			NewIndecies.append(Nodes.index(node))
			NewIndecies.sort()
			SelectedNodes=[Nodes[i] for i in NewIndecies ]
			TempCase=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,SaturationPerPod)
			Number+=1
			NewCases.append(TempCase)
			#print(TempCase.Proximity,TempCase.Imbalance,TempCase.MaxLPP,[Nodes.index(node) for node in SelectedNodes])
			if TestCondition(TempCase,start,VProximity,VImbalance,SaturationPerPod,Timeout)[0]:
				print("Here")
				return NewCases,Number,True
			#print(Proximity,Imbalance,"Scale up "+ str(Upscale))
			NewIndecies.remove(Nodes.index(node))
	return NewCases,Number,False


def FixProximity(Nodes,SelectedNodes,start,TempCase,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,VImbalance,SaturationPerPod,Timeout): 
	Servers={}
	Indecies=[]
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	IdleGateways=[]
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)

	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
		Indecies.append(Index)
	Slow=CalculateSlowPerGateway(Case,LatMat,Nodes,SelectedNodes,TempCase.ProbMat,L,ListReq)
	ToBeAssigned=[]
	i=0
	for k,v in Slow.items():
		if i> len(Slow)/5: 
			break
		ToBeAssigned.append(k)
		i+=1
	Unique,UNodes=GetUnique(TestMat,Indecies,Servers,Nodes)
	ToBeTested=GetScores(Indecies,ToBeAssigned,FullTestMat,Unique,UNodes)
	Cases=[]
	print("FOR LOOP")
	for key,li in ToBeTested.items():
		Indecies.remove(key)
		for i in li:
			#print(NewIndecies)
			Indecies.append(i)
			Indecies.sort()
			#print(NewIndecies)
			SelectedNodes=[Nodes[i] for i in Indecies ]
			#TempCase=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,SaturationPerPod,Number=0)
			#print("Ep:",TempCase.Ep,"Vl0:",TempCase.Vl0,"Vc0",TempCase.Vc0,"MaxLPP:",TempCase.MaxLPP,"Total:",sum(TempCase.RPP),[Nodes.index(node) for node in SelectedNodes])
			Cases.append(TempCase)
			if TestCondition(TempCase,start,VProximity,Ep0,SaturationPerPod,Timeout)[0]:
				return TempCase

			Indecies.remove(i)
		Indecies.append(key)
		Indecies.sort()
	Condition=TestCondition(TempCase,start,VProximity,Ep0,SaturationPerPod,Timeout)[0]
	while(not Condition):
		Indecies=[Nodes.index(n) for n in TempCase.Nodes]
		Cases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
								Timeout,ProbMat,TestMat,
								LatMat,Nodes,ListReq,
								TotalReq,TempCase.Nodes,L,
								Indecies,0,start,
								Reason="Sat")
	if Cases!=0:
		print("entered") 
		Best=GetBest2(Cases,SaturationPerPod,VProximity)
		print(Best.Ep)
	else: 
		Best= TempCase
	return Best

def FixSaturation(Nodes,SelectedNodes,start,TempCase,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,VProximity,Ep0,VImbalance,SaturationPerPod,Timeout): 
	RPPNode=[]
	ToBeTested={}
	Servers={}
	Indecies=[] # selected nodes Indecies
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	IdleGateways=[]
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)

	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
		Indecies.append(Index)
	for i in range(len(TempCase.Nodes)): 
		RPPNode.append((Nodes.index(TempCase.Nodes[i]),TempCase.RPP[i]))
	RPPNode.sort(key=lambda tup: tup[1]) # load per pod indexed by the server node index and sorted in an increasing order
	Unique,UNodes=GetUnique(TestMat,Indecies,Servers,Nodes)
	NexTo=RPPNode[len(RPPNode)-1][0] # node with the highest load (Origin)
	for i in range(len(RPPNode)-1): 
		ToBeReplaced=RPPNode[i][0]
		ProperNodes=GetProperNodes(ToBeReplaced,Indecies,Nodes,FullTestMat,NexTo,Unique,UNodes)
		ToBeTested[ToBeReplaced]=ProperNodes
	Cases=[]
	#print(Indecies)
	for key,li in ToBeTested.items():
		#print("YOHOOOO")
		#print(key)
		Indecies.remove(key)

		for i in li:
			#print(key,i)
			#print(NewIndecies)
			Indecies.append(i)
			Indecies.sort()
			#print(NewIndecies)
			#print(Indecies)
			SelectedNodes=[Nodes[i] for i in Indecies ]
			Temp=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,SaturationPerPod,Number=0)
			#print("Ep:",TempCase.Ep,"Vl0:",TempCase.Vl0,"Vc0",TempCase.Vc0,"MaxLPP:",TempCase.MaxLPP,"Total:",sum(TempCase.RPP),[Nodes.index(node) for node in SelectedNodes])
			Cases.append(Temp)
			if TestCondition(Temp,start,VProximity,Ep0,SaturationPerPod,Timeout)[0]:
				return GetBest2(Cases,SaturationPerPod,VProximity)

			Indecies.remove(i)

		Indecies.append(key)
		Indecies.sort()

	if len(Cases)!=0: 
		Best=GetBest2(Cases,SaturationPerPod,VProximity)
	else: 
		Best=TempCase

	if Best.MaxLPP<SaturationPerPod and VProximity<Best.Proximity:
		#print(Reached) 
		return Best
	else:
		Condition=TestCondition(Best,start,VProximity,Ep0,SaturationPerPod,Timeout)[0]
		while(not Condition):
			Indecies=[Nodes.index(n) for n in Best.Nodes]
			Cases,Number,Condition=AutoScale(VProximity,Ep0,SaturationPerPod,
									Timeout,ProbMat,TestMat,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,
									Indecies,0,start,
									Reason="Sat")
			Best=GetBest2(Cases,SaturationPerPod,VProximity)
		return Best


def CheckscaleDown(Current,ProbMat,LatMat,FullTestMat,Nodes,ListReq,TotalReq,SelectedNodes,L,Ep0,LPC,VProximity=99.9, VImbalance=5,SaturationPerPod=50,Timeout=1): 
	DownScale=3
	Cases=[]
	IdleGateways=[]
	Servers={}
	Indecies=[]
	ActiveNodes=GetActiveNodes(LPC,3)
	lenlpc=len(LPC)
	last=sum(list(LPC[lenlpc-1]))
	pr=0
	if lenlpc>1:
		pr=sum(list(LPC[lenlpc-2]))
	if pr-last>SaturationPerPod/2 and pr>SaturationPerPod : 
		DownScale=len(Current.Nodes)-(TotalReq/SaturationPerPod)-3
	else: 
		DownScale=1
	# for i in ActiveNodes: 
	# 	print(Nodes[i],end=",")
	# print("")
	for i in range(len(Nodes)): 
		if i not in ActiveNodes: 
			IdleGateways.append(i)

	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	#DfPrint(TestMat)
	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
		Indecies.append(Index)
	Unique,UNodes=GetUnique(TestMat,Indecies,Servers,Nodes)
	#print(Unique,UNodes)
	newSelected=SelectedNodes
	i=0
	Number=0
	start=time.time()
	print("DownScale",DownScale)
	while i<DownScale:
		i+=1
		Cases=[]
		if len(newSelected)<=3: 
			break
		for node in newSelected:
			if Nodes.index(node) not in UNodes: 
				Number+=1
				newNodes=ExcludeNode(newSelected,node) 
				TempCase=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,newNodes,L,SaturationPerPod)
				#print("Ep:",TempCase.Ep,"Vl0:",TempCase.Vl0,"Vc0",TempCase.Vc0,"MaxLPP:",TempCase.MaxLPP,"Total:",sum(TempCase.RPP),[node for node in SelectedNodes],TempCase.RPP)
				Cases.append(TempCase)

				#print(TempCase.Proximity,TempCase.MaxLPP)
				#print(TempCase.__dict__)
		if len(Cases)!=0: 
			Best=GetBest2(Cases,SaturationPerPod,VProximity)
			print(Best.Proximity,Best.MaxLPP)
			if TestCondition(Best,start,VProximity,Ep0,SaturationPerPod,10):
				#print(Current.Proximity,Current.RPP) 
				print(Best.Ep,Best.RPP)
				#print("reach!")
				Current=Best
				newSelected=Best.Nodes
			else: 
				break
		else: 
			break


	return Current

def ExcludeNode(SelectedNodes,node): 
	newNodes=[]
	for n in SelectedNodes: 
		if n !=node: 
			newNodes.append(n)
	return newNodes

def GetActiveNodes(LPC,NC): 
	lenlpc=len(LPC)
	LastLPC=[]
	for i in range(NC):
		index=len(LPC)-i-1
		if index> 0: 
			LastLPC.append(list(LPC[index]))
	ActiveNodes=[]
	for l in LastLPC: 
		for i in range(len(l)): 
			if l[i]!=0:
				if i not in ActiveNodes: 
					ActiveNodes.append(i) 
	return ActiveNodes
