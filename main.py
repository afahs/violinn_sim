#!/usr/bin/python3

################## STD LIBS #################
import sys
import pandas as pd
import time
import pickle as pk
import numpy as np
import random
import math
import argparse
import os.path
################## LIBS #####################
sys.path.append('./LoadData')
import hona
import voila
import Create
import honaold as ho
import check
import distrbute
import com

def parseCliOptions():
	parser = argparse.ArgumentParser()

	parser.add_argument( '--n',#imp
		dest       = 'n',  
		type       = int,
		default    = 100,
		help       = 'Number of nodes',
	)

	parser.add_argument( '--ns',#imp
		dest       = 'ns',  
		type       = int,
		default    = 30,
		help       = 'Number of nodes',
	)

	parser.add_argument( '--Algo', #imp
		dest       = 'algoAlt', 
		type       = str,
		default    = "QoSOnly",
		help       = 'The Algorithm to determine best placement',
	)

	parser.add_argument( '--Bias', #imp
		dest       = 'Bias', 
		type       = int,
		default    = 50,
		help       = 'The number of active nodes',
	)

	parser.add_argument( '--L0', #imp
		dest       = 'L0', 
		type       = float,
		default    = 20,
		help       = 'The Latency Threshold in ms',
	)
	
	parser.add_argument( '--C0', #imp
		dest       = 'C0', 
		type       = int,
		default    = 100,
		help       = 'Pod Threshold Capacity',
	)

	parser.add_argument( '--Cs', #imp
		dest       = 'Cs', 
		type       = float,
		default    = 2,
		help       = 'The cost of spare node',
	)
	
	parser.add_argument( '--Ca', #imp
		dest       = 'Ca', 
		type       = float,
		default    = 0.5,
		help       = 'The cost of node activation',
	)
	
	parser.add_argument( '--FitnessAlpha', #imp
		dest       = 'fitnessAlpha', 
		type       = float,
		default    = 0.5,
		help       = 'The alpha value used in the fitness function',
	)


	parser.add_argument( '--Ep0', #imp
		dest       = 'Ep0', 
		type       = float,
		default    = 1,
		help       = 'Threshold in the numbet of slow requests in %',
	)
	parser.add_argument( '--Tol', #imp
		dest       = 'Tol', 
		type       = float,
		default    = 0.2,
		help       = 'Tolerance or Safety [0,1]',
	)
	parser.add_argument( '--NewLoad', #imp
		dest       = 'NewLoad', 
		action     ='store_true',
		help       = 'Get A new Load',
	)
	
	parser.add_argument( '--SpecificLoad', #imp
		dest       = 'SpecificLoad', 
		action     ='store_true',
		help       = 'Get the specific Load',
	)
	
	parser.add_argument( '--NumOfLoads', #imp
		dest       = 'NumOfLoads', 
		type       = int,
		default    = 1,
		help       = 'Number of loads to be tested',
	)

	parser.add_argument( '--Peak', #imp
		dest       = 'Peak', 
		action     ='store_true',
		help       = 'Create a Peak in load',
		)

	parser.add_argument( '--NewPeak', #imp
		dest       = 'NewPeak', 
		action     ='store_true',
		help       = 'Create a new Peak in load',
		)

	parser.add_argument( '--PeakWidth', #imp
		dest       = 'PeakWidth', 
		type       = float,
		default    = 1,
		help       = 'The size of the peak as fucntion of L0',
	)

	parser.add_argument( '--PeakStrength', #imp
		dest       = 'PeakStrength', 
		type       = float,
		default    = 10,
		help       = 'The size of the peak as fucntion of L0',
	)

	parser.add_argument( '--Animate', #imp
		dest       = 'Animate', 
		action     ='store_true',
		help       = 'Animate the Load distribution',
		)

	options        = parser.parse_args()
	return options.__dict__

def Gettime(start,phrase):
	current=time.time()
	print(current-start,phrase)
	return current

def GetSortedNodes(NodeObjects):
	"""
	Returns a list of Sorted Nodes
	"""
	Nodes=[]
	for n in NodeObjects:
		Nodes.append(n.name)
	Nodes=sorted(Nodes)
	return Nodes

def DfPrint(df):
	print(pd.DataFrame(df))

def Comparefunc(L=15,Replicas=10,VProximity=99.9, VImbalance=5,SaturationPerPod=30,Timeout=1):
	start=time.time()
	best=hona.mainfunc(L,Replicas,VProximity,VImbalance,SaturationPerPod,Timeout)
	start=Gettime(start," Time of the new Algo")
	return best
	#print(best.Proximity,best.Imbalance)
	# start=time.time()
	# best=hona.mainfunc(L,Replicas)
	# start=Gettime(start,"new")
	
	print("")
	best,Cases,timeout=ho.GetBest(len(best.Nodes),0,L,vres=VProximity,vfair=VImbalance,vtime=Timeout,vcount=20,method="Mean",algo="Vivaldi",Change=1)
	print("Hona Algo")
	print("selected solution", best.nodes)
	print("Number of Tested cases: ", len(Cases))
	print(best.res,best.fair)
	Gettime(start,"Time of the Hona Algo")

def UpdateBias(NodeObjects,Bias): 
	for node in NodeObjects: 
		node.Idle=True
	Active=[]
	assert Bias<= len(voila.GetNodesOfType("D",NodeObjects)), "Number of Requested Active Nodes \"%d \" Exceeds the number of dedicated Nodes \"%d\"" % Bias % len(voila.GetNodesOfType("D",NodeObjects)) 
	for i in range(Bias): 
		node=random.choice(NodeObjects) 
		while node in Active or node.type=="S":
			node=random.choice(NodeObjects) 
		Active.append(node)
	for node in Active: 
		NodeObjects[NodeObjects.index(node)].Idle=False

	return NodeObjects

#def UpdateSpare(NodeObjects,NS):
#	Spare=[]
#	for i in range(NS):
#		node=random.choice(NodeObjects)
#		while node in Spare:
#			node=random.choice(NodeObjects)
#		Spare.append(node)
#	for node in Spare: 
#		NodeObjects[NodeObjects.index(node)].type="S"
#	return NodeObjects
def GetLatency(node1,node2,Latencies): # Nodeobjects variables
	temp=Latencies[node1.name]
	for k,v in temp:
		if k==node2.name:
			return v

def GetNearbyPeak(NodeObjects,Boundary,Latencies):
	"""	
	Create a neighborhood of nearby nodes
		Peaknode is the center of the peak
		Boundry is the limit in ms
	"""
	Peaknode=random.choice(NodeObjects)
	NearbyPeak=[]
	# count=0
	for node in NodeObjects: 
		if GetLatency(Peaknode,node,Latencies) < Boundary:
			# count+=1
			if node.type!="S": 
				NearbyPeak.append(node)	
	# print(Peaknode.name,Boundary,len(NearbyPeak),count)
	# print([(node.name,GetLatency(Peaknode,node,Latencies)) for node in NearbyPeak])
	return NearbyPeak,Peaknode		

#def Run(C0,L0,Ep0,Bias,n,Tol,f,g):
def Run(options,f,g):
	C0=options["C0"]
	L0=options["L0"]
	Ep0=options["Ep0"]
	Tol=options["Tol"]
	Bias=options["Bias"]
	Peak=options["Peak"]
	NewPeak=options["NewPeak"]
	Boundary=options["PeakWidth"]*L0
	PeakStrength=options["PeakStrength"]
	PeakHours=[18,19,20,21,22]
	Cs=options["Cs"]
	Ca=options["Ca"]
	fitnessAlpha=options["fitnessAlpha"]
	print(Peak,Boundary)
	Pr=False
	NodeObjects=hona.PickleLoad("node.pk")
	Nodes=GetSortedNodes(NodeObjects)
	n=len(Nodes)
	assert n==options["n"], "Inconsistent Number of nodes"
	count=0
	dfhour,dfmin,mapNode,Latencies=distrbute.GetDistribution(Nodes,NewLoad=options["NewLoad"],date='2013-11-25',load='Internet',cycle=1,animate=True,days=2,nodeObjects=NodeObjects,SpecificLoad=options["SpecificLoad"])
	#DfPrint(Latencies)
	dfhour=dfhour.drop([i for i in range(29,48)]) #Only 29h, not two full days
	for index, row in dfhour.iterrows():
		dfhour.loc[index,"Time"]=index
	#print(dfhour)
	NodeObjects=check.UpdateBS(NodeObjects,mapNode)
	check.UpdateLoad(NodeObjects,dfhour,0)
	print(Create.CalculateAverageInterNodeLatencies(Latencies))
	Best=Comparefunc(L=L0,VProximity=99.9, VImbalance=5,SaturationPerPod=C0,Timeout=5)
	newdf=pd.DataFrame(columns=["Time","Replica_size", "Proximity", "Imbalance", "Total_Load", "Max_Load_Per_Pod","Usage","Cost","NumberS"])
	Changes=pd.DataFrame(columns=["Violation", "Changes"])
	Changes.to_pickle("./Changes.pk")
	newdf.set_index('Time', inplace=True)
	newdf.to_pickle("./temp.pk")
	LPC=[]
	if options["Animate"]: 
		com.RunGraphs()
		time.sleep(2)
	Total=0
	TotalLost=0

	if NewPeak:
		NearbyPeak,Peaknode=GetNearbyPeak(NodeObjects,Boundary,Latencies)
		with open('./temp/peak.pk', 'wb') as u:
			pk.dump(NearbyPeak, u, pk.HIGHEST_PROTOCOL)
		print([(node.name,GetLatency(Peaknode,node,Latencies)) for node in NearbyPeak])
	else: 
		NearbyPeak=hona.PickleLoad("peak.pk")
		
	
	for i in range(dfhour.shape[0]):
		NodeObjects=hona.PickleLoad("node.pk")
		Nodes=GetSortedNodes(NodeObjects)
		ListReq,TotalReq=hona.CreateListReq(Nodes,NodeObjects)
		if i in PeakHours and Peak:
			old_list = ListReq.copy()
			for node in NearbyPeak: 
				ListReq[Nodes.index(node.name)]=int(ListReq[Nodes.index(node.name)]*PeakStrength)
		if i==0: 
			LPC=np.array([ListReq])
		else: 
			LPC=np.append(LPC,np.array([ListReq]),axis=0)
		### Algorithm alternative. Possible choices: QoSOnly, QoSMinCost, QoSMinCostImp, QoSFitness
		print("Chosen algorithm for evaluation is: "+options["algoAlt"])
		TempCase=Best
		Best,TotalPerCycle,TotalLostCycle,Pr=check.Check2(i,NodeObjects,LPC,dfhour,dfmin,mapNode,Latencies,Best.Nodes,L0,Ep0,f,g,Tol,Pr,Cs,PeakStrength,Ca,fitnessAlpha,options["algoAlt"],VProximity=99.9, VImbalance=5,SaturationPerPod=C0,Timeout=10,sleep=0)
		
		Cost=voila.CalculateCost(Best,TempCase,NodeObjects,Cs,Ca)
		NumberS=voila.ReturnNumberS(Best.Nodes,NodeObjects)
		row=[len(Best.Nodes),Best.Proximity,Best.Imbalance,sum(Best.RPP),Best.MaxLPP,sum(Best.RPP)*100/(len(Best.Nodes)*C0),Cost,NumberS]
		print("ROW",row)
		newdf=pd.read_pickle("./temp.pk")
		newdf.loc[i+0.5]=row
		newdf.to_pickle("./temp.pk")
		Total+=TotalPerCycle
		TotalLost+=TotalLostCycle
		print([voila.GetNodeType(node,NodeObjects) for node in Best.Nodes])
		#input("go to next cycle")
		#print("TotalReq",Total,"TotalLost",TotalLost,"Percentage", (TotalLost/Total)*100)
		#print(newdf)

def RunMultiple(options): 
	n=options["n"]
	Bias=options["Bias"]
	C0=options["C0"]
	L0=options["L0"]
	Ep0=options["Ep0"]
	Tol=options["Tol"]
	NumOfLoads=options["NumOfLoads"]

	Create.Create(options["n"],numberSpareNodes=options["ns"],Dynamic=True,Dist="uniform",Load=(1000,5000),Dim=3,Dispersion=0.02,Bias=50,mean=300,Variance=100)
	#print(Create.CalculateAverageInterNodeLatencies(lat))
	NodeObjects=hona.PickleLoad("node.pk")
	NodeObjects=UpdateBias(NodeObjects,Bias)
	with open('./temp/node.pk', 'wb') as u:
		pk.dump(NodeObjects,u, pk.HIGHEST_PROTOCOL)

	filename1="_n_"+str(n)+"_L0_"+str(L0)+"_C0_"+"_Bias_"+str(Bias)+"_PS_"+str(options["PeakStrength"])+"_Cs_"+str(options["Cs"])+"_Ca_"+str(options["Ca"])+"_Alpha_"+str(options["fitnessAlpha"])
	filename2="_n_"+str(n)+"_L0_"+str(L0)+"_C0_"+str(C0)+"_Bias_"+str(Bias)+ "_Tol_"+str(Tol)
	g = open("./res/nodes"+filename2+".csv", "w")
	row=["Cycle","Size","Algo","Ep","AEp","Vl0","Vc0","AVc0","MaxLPP","Utilization","TotalReq","Violation","Change","NCases","Ep0","L0","C0","Tol","Cost","NS","PS","Cs","Ca","FitnessAlpha"]
	#if i==0: 
	if not os.path.isfile("./res/res"+filename1+".csv"):
		f = open("./res/res"+filename1+".csv", "w")
		print("entered")
		check.WriteCsvRow(f,row)
	else: 
		f = open("./res/res"+filename1+".csv", "w")
		print("entered")
		check.WriteCsvRow(f,row)

	for i in range(NumOfLoads): 
		alg=["IgnoreS","QoSOnly","QoSMinCost","QoSFitness"]
		PeakStrengthList=[1,2,4,6,8,10,12]
		CsList=[1.9,2,3,10,1,5]
		BiasList=[50,40,60,70]
		NbSpareList=[30,20,40,50,10]
		L0List=[10,20,30,40]
		AlphaList=[0.5,0.7,0.9,0.3,0.1]
		CaList=[0.5,0.7,0.3,0.1,0.9]
		for a in alg: 
			options["algoAlt"]=a
			if a=="IgnoreS": 
				options["NewLoad"]=True
				options["NewPeak"]=True
			#for PeakStrength in PeakStrengthList:
			#	options["PeakStrength"]=PeakStrength
			#	Run(options,f,g)
			#	options["NewLoad"]=False
			#	options["NewPeak"]=False
			#for CsValue in CsList:
			#	options["Cs"]=CsValue
			#	Run(options,f,g)
			#	options["NewLoad"]=False
			#	options["NewPeak"]=False
			#for BiasValue in BiasList: # OBS the load should be changed for any new bias value
			#	options["Bias"]=BiasValue
			#	Run(options,f,g)
			#	options["NewLoad"]=False
			#	options["NewPeak"]=False
			#for NbSpareValue in NbSpareList: # OBS the load should be changed for any new spare value
			#	options["ns"]=NbSpareValue
			#	Run(options,f,g)
			#	options["NewLoad"]=False
			#	options["NewPeak"]=False
			#for L0Value in L0List:
			#	options["L0"]=L0Value
			#	Run(options,f,g)
			#	options["NewLoad"]=False
			#	options["NewPeak"]=False
			#for AlphaValue in AlphaList:
			#	options["fitnessAlpha"]=AlphaValue
			#	Run(options,f,g)
			#	options["NewLoad"]=False
			#	options["NewPeak"]=False
			#for CaValue in CaList:
			#	options["Ca"]=CaValue
			#	Run(options,f,g)
			#	options["NewLoad"]=False
			#	options["NewPeak"]=False
			Run(options,f,g)
			options["NewLoad"]=False
			options["NewPeak"]=False

	f.close()
	g.close()




if __name__ == '__main__':
	options = parseCliOptions()
	if options["NumOfLoads"]!=1: 
		RunMultiple(options)
	else: 
		n=options["n"]
		Bias=options["Bias"]
		C0=options["C0"]
		L0=options["L0"]
		Ep0=options["Ep0"]
		Tol=options["Tol"]
		if options["NewLoad"]: 
			Create.Create(options["n"],numberSpareNodes=options["ns"],Dynamic=True,Dist="uniform",Load=(1000,5000),Dim=3,Dispersion=0.02,Bias=50,mean=300,Variance=100)
			#print(Create.CalculateAverageInterNodeLatencies(lat))
			NodeObjects=hona.PickleLoad("node.pk")
			NodeObjects=UpdateBias(NodeObjects,Bias)
			with open('./temp/node.pk', 'wb') as u:
				pk.dump(NodeObjects,u, pk.HIGHEST_PROTOCOL)
		#loadperCycle=np.array()
		# C0=100
		# L0=25
		# Ep0=1
		# Bias=50
		# n=100
		# Tol=0.2
		#PS=.5 # Percentage of spare nodes
		#NS= math.floor(n*PS)  # number of spare nodes

		#for i in range(6):
		filename1="_n_"+str(n)+"_L0_"+str(L0)+"_C0_"+"_Bias_"+str(Bias)+"_PS_"+str(options["PeakStrength"])+"_Cs_"+str(options["Cs"])+"_Ca_"+str(options["Ca"])+"_Alpha_"+str(options["fitnessAlpha"])
		filename2="_n_"+str(n)+"_L0_"+str(L0)+"_C0_"+str(C0)+"_Bias_"+str(Bias)+ "_Tol_"+str(Tol)
		g = open("./res/nodes"+filename2+"Cost=10"+".csv", "w")
		row=["Cycle","Size","Algo","Ep","AEp","Vl0","Vc0","AVc0","MaxLPP","Utilization","TotalReq","Violation","Change","NCases","Ep0","L0","C0","Tol","Cost","NS","PS","Cs","Ca","FitnessAlpha"]
		#if i==0: 
		if not os.path.isfile("./res/res"+filename1+"Cost=10"+".csv"):
			f = open("./res/res"+filename1+"Cost=10"+".csv", "w")
			print("entered")
			check.WriteCsvRow(f,row)
		else: 
			f = open("./res/res"+filename1+"Cost=10"+".csv", "w")
			print("entered")
			check.WriteCsvRow(f,row)
		#	else: 
		#		f = open("./res/res"+filename1+".csv", "a")
		#	C0=80+10*i
			#Run(C0,L0,Ep0,Bias,n,Tol,f,g)
		alg=["IgnoreS","QoSOnly","QoSMinCost","QoSFitness"]
		#alg=["QoSMinCost"]
		for a in alg: 
			options["algoAlt"]=a
			Run(options,f,g)
		f.close()
		g.close()
